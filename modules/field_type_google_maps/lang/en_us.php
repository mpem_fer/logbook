<?php

$L = array();
$L["module_name"] = "Google Maps Field";
$L["module_description"] = "This module lets you choose a Google Maps field type to let you visualize an address on a map.";
