FROM php:7.2-apache

RUN a2enmod rewrite

RUN docker-php-ext-install mysqli

#RUN docker-php-ext-install pdo pdo_mysql

COPY ./ /var/www/html/

#RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf

RUN service apache2 restart