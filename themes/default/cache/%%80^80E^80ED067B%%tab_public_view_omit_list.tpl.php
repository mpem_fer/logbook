<?php /* Smarty version 2.6.18, created on 2016-04-08 10:51:07
         compiled from C:%5Cxampp%5Chtdocs%5Clogbook/themes/default/admin/forms/tab_public_view_omit_list.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'upper', 'C:\\xampp\\htdocs\\logbook/themes/default/admin/forms/tab_public_view_omit_list.tpl', 6, false),array('function', 'ft_include', 'C:\\xampp\\htdocs\\logbook/themes/default/admin/forms/tab_public_view_omit_list.tpl', 8, false),array('function', 'clients_dropdown', 'C:\\xampp\\htdocs\\logbook/themes/default/admin/forms/tab_public_view_omit_list.tpl', 27, false),)), $this); ?>
  <div class="previous_page_icon">
    <a href="edit.php?page=edit_view&form_id=<?php echo $this->_tpl_vars['form_id']; ?>
&view_id=<?php echo $this->_tpl_vars['view_id']; ?>
"><img src="<?php echo $this->_tpl_vars['images_url']; ?>
/up.jpg" title="<?php echo $this->_tpl_vars['LANG']['phrase_previous_page']; ?>
"
      alt="<?php echo $this->_tpl_vars['LANG']['phrase_previous_page']; ?>
" border="0" /></a>
  </div>

  <div class="subtitle underline margin_top_large"><?php echo ((is_array($_tmp=$this->_tpl_vars['LANG']['phrase_public_view_omit_list'])) ? $this->_run_mod_handler('upper', true, $_tmp) : smarty_modifier_upper($_tmp)); ?>
</div>

  <?php echo smarty_function_ft_include(array('file' => "messages.tpl"), $this);?>


  <div class="margin_bottom_large">
    <?php echo $this->_tpl_vars['LANG']['text_public_view_omit_list_page']; ?>

  </div>

  <form method="post" action="<?php echo $this->_tpl_vars['same_page']; ?>
" onsubmit="ft.select_all('selected_client_ids[]')">
    <input type="hidden" name="form_id" value="<?php echo $this->_tpl_vars['form_id']; ?>
" />
    <input type="hidden" name="view_id" value="<?php echo $this->_tpl_vars['view_id']; ?>
" />
    <input type="hidden" name="page" value="public_view_omit_list" />

    <table cellpadding="1" cellspacing="0" class="list_table">
    <tr>
      <td class="medium_grey pad_left_small"><?php echo $this->_tpl_vars['LANG']['phrase_clients_can_access_view']; ?>
</td>
      <td></td>
      <td class="medium_grey pad_left_small"><?php echo $this->_tpl_vars['LANG']['phrase_clients_cannot_access_view']; ?>
</td>
    </tr>
    <tr>
      <td>
        <?php echo smarty_function_clients_dropdown(array('name_id' => "available_client_ids[]",'multiple' => 'true','multiple_action' => 'hide','clients' => $this->_tpl_vars['view_omit_list'],'size' => '4','style' => "width: 280px"), $this);?>

      </td>
      <td align="center" valign="middle" width="100">
        <input type="button" value="<?php echo $this->_tpl_vars['LANG']['word_add_uc_rightarrow']; ?>
"
          onclick="ft.move_options('available_client_ids[]', 'selected_client_ids[]');" /><br />
        <input type="button" value="<?php echo $this->_tpl_vars['LANG']['word_remove_uc_leftarrow']; ?>
"
          onclick="ft.move_options('selected_client_ids[]', 'available_client_ids[]');" />
      </td>
      <td>
        <?php echo smarty_function_clients_dropdown(array('name_id' => "selected_client_ids[]",'multiple' => 'true','multiple_action' => 'show','clients' => $this->_tpl_vars['view_omit_list'],'size' => '4','style' => "width: 280px"), $this);?>

      </td>
    </tr>
    </table>

    <p>
      <input type="submit" name="update_public_view_omit_list" value="<?php echo $this->_tpl_vars['LANG']['word_update']; ?>
" />
      <input type="button" value="<?php echo $this->_tpl_vars['LANG']['phrase_clear_omit_list']; ?>
" class="blue" onclick="page_ns.clear_omit_list()" />
    </p>

  </form>