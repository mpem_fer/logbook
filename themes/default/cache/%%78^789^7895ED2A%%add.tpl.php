<?php /* Smarty version 2.6.18, created on 2016-04-14 06:01:25
         compiled from C:%5Cxampp%5Chtdocs%5Clogbook/modules/extended_client_fields/templates/add.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'upper', 'C:\\xampp\\htdocs\\logbook/modules/extended_client_fields/templates/add.tpl', 91, false),array('modifier', 'escape', 'C:\\xampp\\htdocs\\logbook/modules/extended_client_fields/templates/add.tpl', 134, false),array('function', 'option_list_dropdown', 'C:\\xampp\\htdocs\\logbook/modules/extended_client_fields/templates/add.tpl', 114, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'modules_header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

  <table cellpadding="0" cellspacing="0">
  <tr>
    <td width="45"><a href="index.php"><img src="images/icon_extended_client_fields.gif" border="0" width="34" height="34" /></a></td>
    <td class="title">
      <a href="../../admin/modules"><?php echo $this->_tpl_vars['LANG']['word_modules']; ?>
</a>
      <span class="joiner">&raquo;</span>
      <a href="./"><?php echo $this->_tpl_vars['L']['module_name']; ?>
</a>
      <span class="joiner">&raquo;</span>
      <?php echo $this->_tpl_vars['LANG']['phrase_add_field']; ?>

    </td>
  </tr>
  </table>

  <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'messages.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

  <form action="<?php echo $this->_tpl_vars['same_page']; ?>
" method="post" onsubmit="return rsv.validate(this, rules)">
    <input type="hidden" name="num_rows" id="num_rows" value="0" />

    <table cellspacing="1" cellpadding="1" border="0" class="margin_bottom_large">
    <tr>
      <td width="150"><?php echo $this->_tpl_vars['L']['phrase_page_and_location']; ?>
</td>
      <td>
        <select name="template_hook">
          <option value=""><?php echo $this->_tpl_vars['LANG']['phrase_please_select']; ?>
</option>
          <optgroup label="<?php echo $this->_tpl_vars['L']['phrase_main_account_page']; ?>
">
            <option value="edit_client_main_top"><?php echo $this->_tpl_vars['L']['word_top']; ?>
</option>
            <option value="edit_client_main_middle"><?php echo $this->_tpl_vars['L']['word_middle']; ?>
</option>
            <option value="edit_client_main_bottom"><?php echo $this->_tpl_vars['L']['word_bottom']; ?>
</option>
          </optgroup>
          <optgroup label="<?php echo $this->_tpl_vars['L']['phrase_settings_page']; ?>
">
            <option value="edit_client_settings_top"><?php echo $this->_tpl_vars['L']['word_top']; ?>
</option>
            <option value="edit_client_settings_bottom"><?php echo $this->_tpl_vars['L']['word_bottom']; ?>
</option>
          </optgroup>
        </select>
      </td>
    </tr>
    <tr>
      <td valign="top"><?php echo $this->_tpl_vars['LANG']['phrase_admin_only']; ?>
</td>
      <td valign="top">
        <input type="radio" name="admin_only" id="ao1" value="yes" />
          <label for="ao1"><?php echo $this->_tpl_vars['LANG']['word_yes']; ?>
</label>
        <input type="radio" name="admin_only" id="ao2" value="no" checked />
          <label for="ao2"><?php echo $this->_tpl_vars['LANG']['word_no']; ?>
</label>
        <div class="medium_grey">
          <?php echo $this->_tpl_vars['L']['notify_admin_only_field_explanation']; ?>

        </div>
      </td>
    </tr>
    <tr>
      <td><?php echo $this->_tpl_vars['L']['phrase_field_label']; ?>
</td>
      <td><input type="text" name="field_label" style="width:550px" /></td>
    </tr>
    <tr>
      <td><?php echo $this->_tpl_vars['L']['phrase_field_type']; ?>
</td>
      <td>
        <select name="field_type" id="field_type">
          <option value="" selected><?php echo $this->_tpl_vars['LANG']['phrase_please_select']; ?>
</option>
          <option value="textbox"><?php echo $this->_tpl_vars['LANG']['word_textbox']; ?>
</option>
          <option value="textarea"><?php echo $this->_tpl_vars['LANG']['word_textarea']; ?>
</option>
          <option value="password"><?php echo $this->_tpl_vars['LANG']['word_password']; ?>
</option>
          <option value="radios"><?php echo $this->_tpl_vars['LANG']['phrase_radio_buttons']; ?>
</option>
          <option value="checkboxes"><?php echo $this->_tpl_vars['LANG']['word_checkboxes']; ?>
</option>
          <option value="select"><?php echo $this->_tpl_vars['LANG']['word_dropdown']; ?>
</option>
          <option value="multi-select"><?php echo $this->_tpl_vars['LANG']['phrase_multi_select']; ?>
</option>
        </select>
      </td>
    </tr>
    <tr>
      <td><?php echo $this->_tpl_vars['L']['phrase_default_value']; ?>
</td>
      <td><input type="text" name="default_value" style="width:550px" /></td>
    </tr>
    <tr>
      <td>
        <input type="checkbox" name="is_required" id="is_required" />
          <label for="is_required"><?php echo $this->_tpl_vars['L']['phrase_required_field']; ?>
</label>
      </td>
      <td>
        <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
          <td class="margin_right_large"><?php echo $this->_tpl_vars['L']['phrase_error_string_c']; ?>
</td>
          <td align="right"><input type="text" name="error_string" style="width:470px" value="" /></td>
        </tr>
        </table>
      </td>
    </tr>
    </table>

    <div id="field_options_div" style="display:none">
      <div class="margin_bottom_large subtitle underline"><?php echo ((is_array($_tmp=$this->_tpl_vars['LANG']['phrase_field_options'])) ? $this->_run_mod_handler('upper', true, $_tmp) : smarty_modifier_upper($_tmp)); ?>
</div>
      <table>
        <tr>
          <td width="140"><?php echo $this->_tpl_vars['L']['word_orientation']; ?>
</td>
          <td>
            <input type="radio" name="field_orientation" id="fo1" value="horizontal" checked />
              <label for="fo1"><?php echo $this->_tpl_vars['LANG']['word_horizontal']; ?>
</label>
            <input type="radio" name="field_orientation" id="fo2" value="vertical" />
              <label for="fo2"><?php echo $this->_tpl_vars['LANG']['word_vertical']; ?>
</label>
            <input type="radio" name="field_orientation" id="fo3" value="na" />
              <label for="fo3"><?php echo $this->_tpl_vars['LANG']['word_na']; ?>
</label>
          </td>
        </tr>
        <tr>
          <td width="140" valign="top"><?php echo $this->_tpl_vars['L']['phrase_field_option_source']; ?>
</td>
          <td>
            <table  class="list_table">
            <tr>
              <td width="140">
                <input type="radio" name="option_source" id="os1" value="option_list" checked />
                <label for="os1"><?php echo $this->_tpl_vars['LANG']['phrase_option_list']; ?>
</label>
              </td>
              <td>
                <?php echo smarty_function_option_list_dropdown(array('name_id' => 'option_list_id'), $this);?>

              </td>
            </tr>
            <tr>
              <td valign="top">
                <input type="radio" name="option_source" id="os2" value="custom_list" />
                <label for="os2"><?php echo $this->_tpl_vars['L']['phrase_custom_list']; ?>
</label>
              </td>
              <td>
                <table cellspacing="1" cellpadding="0" id="field_options_table" class="list_table" style="width: 448px">
                <tbody>
                  <tr>
                    <th width="40"> </th>
                    <th><?php echo $this->_tpl_vars['LANG']['phrase_display_text']; ?>
</th>
                    <th class="del"></th>
                  </tr>
                  <?php $_from = $this->_tpl_vars['field_info']['options']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['row'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['row']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['option']):
        $this->_foreach['row']['iteration']++;
?>
                    <?php $this->assign('count', $this->_foreach['row']['iteration']); ?>
                      <tr id="row_<?php echo $this->_tpl_vars['count']; ?>
">
                        <td class="medium_grey" align="center" id="field_option_<?php echo $this->_tpl_vars['count']; ?>
_order"><?php echo $this->_tpl_vars['count']; ?>
</td>
                        <td><input type="text" style="width:99%" name="field_option_text_<?php echo $this->_tpl_vars['count']; ?>
" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['option']['option_text'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" /></td>
                        <td class="del"><a href="#" onclick="ecf_ns.delete_field_option(<?php echo $this->_tpl_vars['count']; ?>
)"></a></td>
                      </tr>
                    <?php endforeach; endif; unset($_from); ?>
                  </tbody>
                </table>

                <div>
                  <a href="#" onclick="ecf_ns.add_field_option(null, null)"><?php echo $this->_tpl_vars['LANG']['phrase_add_row']; ?>
</a>
                </div>
              </td>
            </tr>
            </table>

          </td>
        </tr>
      </table>
    </div>

    <p>
      <input type="submit" name="add" value="<?php echo $this->_tpl_vars['L']['phrase_add_field']; ?>
" />
    </p>

  </form>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'modules_footer.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>