<?php /* Smarty version 2.6.18, created on 2016-04-14 06:03:25
         compiled from C:%5Cxampp%5Chtdocs%5Clogbook/modules/extended_client_fields/smarty/section_html.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'escape', 'C:\\xampp\\htdocs\\logbook/modules/extended_client_fields/smarty/section_html.tpl', 18, false),array('modifier', 'in_array', 'C:\\xampp\\htdocs\\logbook/modules/extended_client_fields/smarty/section_html.tpl', 56, false),array('function', 'display_option_list', 'C:\\xampp\\htdocs\\logbook/modules/extended_client_fields/smarty/section_html.tpl', 45, false),)), $this); ?>

<?php if ($this->_tpl_vars['title']): ?>
  <p class="subtitle margin_bottom_large"><?php echo $this->_tpl_vars['title']; ?>
</p>
<?php else: ?>
  <div class="margin_bottom_large"> </div>
<?php endif; ?>

<table class="list_table margin_bottom_large" cellpadding="0" cellspacing="1">
<?php $_from = $this->_tpl_vars['fields']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['extended_field']):
?>
  <?php $this->assign('client_field_id', $this->_tpl_vars['extended_field']['client_field_id']); ?>
  <tr>
    <td class="red" align="center" width="15">
      <?php if ($this->_tpl_vars['extended_field']['is_required'] == 'yes'): ?>
        *
        <script>
        <?php if ($this->_tpl_vars['extended_field']['field_type'] == 'checkboxes' || $this->_tpl_vars['extended_field']['field_type'] == "multi-select"): ?>
          rules.push("required,ecf_<?php echo $this->_tpl_vars['client_field_id']; ?>
[],<?php echo ((is_array($_tmp=$this->_tpl_vars['extended_field']['error_string'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
");
        <?php else: ?>
          rules.push("required,ecf_<?php echo $this->_tpl_vars['client_field_id']; ?>
,<?php echo ((is_array($_tmp=$this->_tpl_vars['extended_field']['error_string'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
");
        <?php endif; ?>
        </script>
      <?php endif; ?>
    </td>
    <td class="pad_left_small" width="180"><?php echo $this->_tpl_vars['extended_field']['field_label']; ?>
</td>
    <td>
      <?php if ($this->_tpl_vars['extended_field']['field_type'] == 'textbox'): ?>
        <input type="text" name="ecf_<?php echo $this->_tpl_vars['client_field_id']; ?>
" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['extended_field']['content'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" size="50" />
      <?php elseif ($this->_tpl_vars['extended_field']['field_type'] == 'textarea'): ?>
        <textarea name="ecf_<?php echo $this->_tpl_vars['client_field_id']; ?>
" style="width:98%; height: 60px"><?php echo $this->_tpl_vars['extended_field']['content']; ?>
</textarea>
      <?php elseif ($this->_tpl_vars['extended_field']['field_type'] == 'password'): ?>
        <input type="password" name="ecf_<?php echo $this->_tpl_vars['client_field_id']; ?>
" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['extended_field']['content'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" size="20" />
      <?php elseif ($this->_tpl_vars['extended_field']['field_type'] == 'radios'): ?>

        <?php if ($this->_tpl_vars['extended_field']['option_source'] == 'custom_list'): ?>
          <?php $_from = $this->_tpl_vars['extended_field']['options']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['row'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['row']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k2'] => $this->_tpl_vars['option']):
        $this->_foreach['row']['iteration']++;
?>
            <?php $this->assign('count', $this->_foreach['row']['iteration']); ?>
            <?php $this->assign('escaped_value', $this->_tpl_vars['option']['option_text']); ?>
            <input type="radio" name="ecf_<?php echo $this->_tpl_vars['client_field_id']; ?>
" id="eft_<?php echo $this->_tpl_vars['client_field_id']; ?>
_<?php echo $this->_tpl_vars['count']; ?>
" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['option']['option_text'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
"
              <?php if ($this->_tpl_vars['escaped_value'] == $this->_tpl_vars['extended_field']['content']): ?>checked<?php endif; ?> />
              <label for="eft_<?php echo $this->_tpl_vars['client_field_id']; ?>
_<?php echo $this->_tpl_vars['count']; ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['option']['option_text'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</label>
              <?php if ($this->_tpl_vars['extended_field']['field_orientation'] == 'vertical'): ?><br /><?php endif; ?>
          <?php endforeach; endif; unset($_from); ?>
        <?php else: ?>
          <?php echo smarty_function_display_option_list(array('option_list_id' => $this->_tpl_vars['extended_field']['option_list_id'],'name' => "ecf_".($this->_tpl_vars['client_field_id']),'format' => 'radios','default_value' => $this->_tpl_vars['extended_field']['content']), $this);?>

        <?php endif; ?>

      <?php elseif ($this->_tpl_vars['extended_field']['field_type'] == 'checkboxes'): ?>

        <?php if ($this->_tpl_vars['extended_field']['option_source'] == 'custom_list'): ?>
          <?php $_from = $this->_tpl_vars['extended_field']['options']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['row'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['row']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k2'] => $this->_tpl_vars['option']):
        $this->_foreach['row']['iteration']++;
?>
            <?php $this->assign('count', $this->_foreach['row']['iteration']); ?>
            <?php $this->assign('escaped_value', ((is_array($_tmp=$this->_tpl_vars['option']['option_text'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp))); ?>
            <input type="checkbox" name="ecf_<?php echo $this->_tpl_vars['client_field_id']; ?>
[]" id="eft_<?php echo $this->_tpl_vars['client_field_id']; ?>
_<?php echo $this->_tpl_vars['count']; ?>
" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['option']['option_text'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
"
              <?php if (((is_array($_tmp=$this->_tpl_vars['escaped_value'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['extended_field']['content']) : in_array($_tmp, $this->_tpl_vars['extended_field']['content']))): ?>checked<?php endif; ?> />
              <label for="eft_<?php echo $this->_tpl_vars['client_field_id']; ?>
_<?php echo $this->_tpl_vars['count']; ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['option']['option_text'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</label>
              <?php if ($this->_tpl_vars['extended_field']['field_orientation'] == 'vertical'): ?><br /><?php endif; ?>
          <?php endforeach; endif; unset($_from); ?>
        <?php else: ?>
          <?php echo smarty_function_display_option_list(array('option_list_id' => $this->_tpl_vars['extended_field']['option_list_id'],'name' => "ecf_".($this->_tpl_vars['client_field_id']),'format' => 'checkboxes','default_value' => $this->_tpl_vars['extended_field']['content']), $this);?>

        <?php endif; ?>

      <?php elseif ($this->_tpl_vars['extended_field']['field_type'] == 'select'): ?>

        <?php if ($this->_tpl_vars['extended_field']['option_source'] == 'custom_list'): ?>
          <select name="ecf_<?php echo $this->_tpl_vars['client_field_id']; ?>
">
            <?php $_from = $this->_tpl_vars['extended_field']['options']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k2'] => $this->_tpl_vars['option']):
?>
              <?php $this->assign('escaped_value', ((is_array($_tmp=$this->_tpl_vars['option']['option_text'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp))); ?>
              <option value="<?php echo ((is_array($_tmp=$this->_tpl_vars['option']['option_text'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" <?php if ($this->_tpl_vars['escaped_value'] == $this->_tpl_vars['extended_field']['content']): ?>selected<?php endif; ?>><?php echo $this->_tpl_vars['option']['option_text']; ?>
</option>
             <?php endforeach; endif; unset($_from); ?>
          </select>
        <?php else: ?>
          <?php echo smarty_function_display_option_list(array('option_list_id' => $this->_tpl_vars['extended_field']['option_list_id'],'name' => "ecf_".($this->_tpl_vars['client_field_id']),'format' => 'select','default_value' => $this->_tpl_vars['extended_field']['content']), $this);?>

        <?php endif; ?>

      <?php elseif ($this->_tpl_vars['extended_field']['field_type'] == "multi-select"): ?>

        <?php if ($this->_tpl_vars['extended_field']['option_source'] == 'custom_list'): ?>
          <select name="ecf_<?php echo $this->_tpl_vars['client_field_id']; ?>
[]" multiple size="4">
            <?php $_from = $this->_tpl_vars['extended_field']['options']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k2'] => $this->_tpl_vars['option']):
?>
              <?php $this->assign('escaped_value', ((is_array($_tmp=$this->_tpl_vars['option']['option_text'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp))); ?>
              <option value="<?php echo ((is_array($_tmp=$this->_tpl_vars['option']['option_text'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
"
                <?php if (((is_array($_tmp=$this->_tpl_vars['escaped_value'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['extended_field']['content']) : in_array($_tmp, $this->_tpl_vars['extended_field']['content']))): ?>selected<?php endif; ?>><?php echo $this->_tpl_vars['option']['option_text']; ?>
</option>
             <?php endforeach; endif; unset($_from); ?>
          </select>
        <?php else: ?>
          <?php echo smarty_function_display_option_list(array('option_list_id' => $this->_tpl_vars['extended_field']['option_list_id'],'name' => "ecf_".($this->_tpl_vars['client_field_id']),'format' => "multi-select",'default_value' => $this->_tpl_vars['extended_field']['content']), $this);?>

        <?php endif; ?>

      <?php endif; ?>
    </td>
  </tr>
<?php endforeach; endif; unset($_from); ?>
</table>