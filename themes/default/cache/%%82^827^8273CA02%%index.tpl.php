<?php /* Smarty version 2.6.18, created on 2016-04-20 10:58:30
         compiled from C:%5Cxampp%5Chtdocs%5Clogbook/modules/system_check/templates/index.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'modules_header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

  <table cellpadding="0" cellspacing="0">
  <tr>
    <td width="45"><img src="images/icon.png" width="34" height="34" /></td>
    <td class="title">
      <a href="../../admin/modules"><?php echo $this->_tpl_vars['LANG']['word_modules']; ?>
</a>
      <span class="joiner">&raquo;</span>
      <?php echo $this->_tpl_vars['L']['module_name']; ?>

    </td>
  </tr>
  </table>

  <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "messages.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

  <div class="margin_bottom_large">
    <?php echo $this->_tpl_vars['L']['text_module_intro']; ?>

  </div>

  <hr size="1" />

  <table cellspacing="0" class="index_link_table">
  <tr>
    <td valign="top" width="160"><a href="files.php"><?php echo $this->_tpl_vars['L']['phrase_file_verification']; ?>
</a></td>
    <td><?php echo $this->_tpl_vars['L']['text_file_check']; ?>
 <?php echo $this->_tpl_vars['L']['text_problems_identified_not_fixed']; ?>
</td>
  </tr>
  <tr>
    <td valign="top"><a href="tables.php"><?php echo $this->_tpl_vars['L']['phrase_table_verification']; ?>
</a></td>
    <td><?php echo $this->_tpl_vars['L']['text_table_verification_intro']; ?>
 <?php echo $this->_tpl_vars['L']['text_problems_identified_not_fixed']; ?>
</td>
  </tr>
  <tr>
    <td valign="top"><a href="hooks.php"><?php echo $this->_tpl_vars['L']['phrase_hook_verification']; ?>
</a></td>
    <td><?php echo $this->_tpl_vars['L']['text_hook_verification_intro']; ?>
 <?php echo $this->_tpl_vars['L']['text_problems_identified_and_fixed']; ?>
</td>
  </tr>
  <tr>
    <td valign="top" class="rowN"><a href="orphans.php"><?php echo $this->_tpl_vars['L']['phrase_orphan_clean_up']; ?>
</a></td>
    <td><?php echo $this->_tpl_vars['L']['text_orphan_desc_short']; ?>
 <?php echo $this->_tpl_vars['L']['text_problems_identified_and_fixed']; ?>
</td>
  </tr>
  </table>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'modules_footer.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>