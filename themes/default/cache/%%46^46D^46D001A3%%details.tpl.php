<?php /* Smarty version 2.6.18, created on 2016-06-02 07:55:09
         compiled from C:%5Cxampp%5Chtdocs%5Clogbook/modules/client_audit/templates/details.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'display_account_name', 'C:\\xampp\\htdocs\\logbook/modules/client_audit/templates/details.tpl', 42, false),array('function', 'display_form_name', 'C:\\xampp\\htdocs\\logbook/modules/client_audit/templates/details.tpl', 82, false),array('function', 'display_view_name', 'C:\\xampp\\htdocs\\logbook/modules/client_audit/templates/details.tpl', 86, false),array('modifier', 'custom_format_date', 'C:\\xampp\\htdocs\\logbook/modules/client_audit/templates/details.tpl', 66, false),array('modifier', 'in_array', 'C:\\xampp\\htdocs\\logbook/modules/client_audit/templates/details.tpl', 81, false),array('modifier', 'array_key_exists', 'C:\\xampp\\htdocs\\logbook/modules/client_audit/templates/details.tpl', 186, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'modules_header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

  <table cellpadding="0" cellspacing="0">
  <tr>
    <td width="45"><a href="index.php"><img src="images/icon_client_audit.gif" border="0" width="34" height="34" /></a></td>
    <td class="title">
      <a href="../../admin/modules"><?php echo $this->_tpl_vars['LANG']['word_modules']; ?>
</a>
      <span class="joiner">&raquo;</span>
      <a href="./"><?php echo $this->_tpl_vars['L']['module_name']; ?>
</a>
      <span class="joiner">&raquo;</span>
      <?php echo $this->_tpl_vars['L']['word_changes']; ?>

    </td>
  </tr>
  </table>

  <table cellspacing="0" cellpadding="0" class="margin_bottom_large">
    <tr>
      <td width="80" class="nowrap">
        <?php if ($this->_tpl_vars['nav_info']['previous_change_id'] == ""): ?>
          <span class="light_grey"><?php echo $this->_tpl_vars['LANG']['word_previous_leftarrow']; ?>
</span>
        <?php else: ?>
          <a href="?change_id=<?php echo $this->_tpl_vars['nav_info']['previous_change_id']; ?>
"><?php echo $this->_tpl_vars['LANG']['word_previous_leftarrow']; ?>
</a>
        <?php endif; ?>
      </td>
      <td width="150" class="nowrap"><a href="./"><?php echo $this->_tpl_vars['LANG']['phrase_back_to_search_results']; ?>
</a></td>
      <td>
        <?php if ($this->_tpl_vars['nav_info']['next_change_id'] == ""): ?>
          <span class="light_grey"><?php echo $this->_tpl_vars['LANG']['word_next_rightarrow']; ?>
</span>
        <?php else: ?>
          <a href="?change_id=<?php echo $this->_tpl_vars['nav_info']['next_change_id']; ?>
"><?php echo $this->_tpl_vars['LANG']['word_next_rightarrow']; ?>
</a>
        <?php endif; ?>
      </td>
    </tr>
  </table>

  <div class="notify">
    <div style="padding: 6px">
      <table cellspacing="0" cellpadding="0">
      <tr>
        <td width="120" class="bold"><?php echo $this->_tpl_vars['LANG']['word_client']; ?>
</td>
        <td>
          <a href="../../admin/clients/edit.php?client_id=<?php echo $this->_tpl_vars['change_info']['account_id']; ?>
"><?php echo smarty_function_display_account_name(array('account_id' => $this->_tpl_vars['change_info']['account_id']), $this);?>
</a>
        </td>
      </tr>
      <tr>
        <td><?php echo $this->_tpl_vars['L']['phrase_change_type']; ?>
</td>
        <td>
        <?php if ($this->_tpl_vars['change_info']['change_type'] == 'permissions'): ?>
          <span class="ct_permissions">Permissions updated</span>
        <?php elseif ($this->_tpl_vars['change_info']['change_type'] == 'admin_update'): ?>
          <span class="ct_admin_update">Updated by admin</span>
        <?php elseif ($this->_tpl_vars['change_info']['change_type'] == 'client_update'): ?>
          <span class="ct_client_update">Updated by client</span>
        <?php elseif ($this->_tpl_vars['change_info']['change_type'] == 'account_created'): ?>
          <span class="ct_account_created">Account created</span>
        <?php elseif ($this->_tpl_vars['change_info']['change_type'] == 'account_deleted'): ?>
          <span class="ct_account_deleted">Account deleted</span>
        <?php elseif ($this->_tpl_vars['change_info']['change_type'] == 'account_disabled_from_failed_logins'): ?>
          <span class="ct_account_disabled_from_failed_logins">Account disabled from failed logins</span>
        <?php endif; ?>
        </td>
      </tr>
      <tr>
        <td><?php echo $this->_tpl_vars['L']['phrase_change_date']; ?>
</td>
        <td>
          <?php echo ((is_array($_tmp=$this->_tpl_vars['change_info']['change_date'])) ? $this->_run_mod_handler('custom_format_date', true, $_tmp, $this->_tpl_vars['SESSION']['account']['timezone_offset'], $this->_tpl_vars['SESSION']['account']['date_format']) : smarty_modifier_custom_format_date($_tmp, $this->_tpl_vars['SESSION']['account']['timezone_offset'], $this->_tpl_vars['SESSION']['account']['date_format'])); ?>

        </td>
      </tr>
      </table>
    </div>
  </div>

  <?php if ($this->_tpl_vars['change_info']['change_type'] == 'permissions'): ?>

    <p>
      <?php echo $this->_tpl_vars['L']['text_permissions_desc']; ?>

    </p>

    <table cellspacing="1" cellpadding="0" class="list_table">
    <?php $_from = $this->_tpl_vars['permissions']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['form_id'] => $this->_tpl_vars['view_ids']):
?>
      <tr<?php if (((is_array($_tmp=$this->_tpl_vars['form_id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['added_forms']) : in_array($_tmp, $this->_tpl_vars['added_forms']))): ?> class="added"<?php endif; ?>>
        <td class="pad_left"><a href="../../admin/forms/edit.php?form_id=<?php echo $this->_tpl_vars['form_id']; ?>
&page=main"><?php echo smarty_function_display_form_name(array('form_id' => $this->_tpl_vars['form_id']), $this);?>
</a></td>
        <td class="pad_left">
         <?php $_from = $this->_tpl_vars['all_form_views'][$this->_tpl_vars['form_id']]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['view_info']):
?>
           <?php if (((is_array($_tmp=$this->_tpl_vars['view_info']['view_id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['view_ids']) : in_array($_tmp, $this->_tpl_vars['view_ids']))): ?>
             <div<?php if (((is_array($_tmp=$this->_tpl_vars['view_info']['view_id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['added_views']) : in_array($_tmp, $this->_tpl_vars['added_views']))): ?> class="added"<?php endif; ?>><a href="../../admin/forms/edit.php?page=edit_view&form_id=<?php echo $this->_tpl_vars['form_id']; ?>
&view_id=<?php echo $this->_tpl_vars['view_info']['view_id']; ?>
"><?php echo smarty_function_display_view_name(array('view_id' => $this->_tpl_vars['view_info']['view_id']), $this);?>
</a></div>
           <?php endif; ?>
           <?php if (((is_array($_tmp=$this->_tpl_vars['view_info']['view_id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['removed_views']) : in_array($_tmp, $this->_tpl_vars['removed_views']))): ?>
             <div class="removed"><a href="../../admin/forms/edit.php?page=edit_view&form_id=<?php echo $this->_tpl_vars['form_id']; ?>
&view_id=<?php echo $this->_tpl_vars['view_info']['view_id']; ?>
"><?php echo smarty_function_display_view_name(array('view_id' => $this->_tpl_vars['view_info']['view_id']), $this);?>
</a></div>
           <?php endif; ?>
         <?php endforeach; endif; unset($_from); ?>
        </td>
      </tr>
    <?php endforeach; endif; unset($_from); ?>
        <?php $_from = $this->_tpl_vars['removed_forms']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['form_id']):
?>
      <tr class="deleted">
        <td class="pad_left"><a href="../../admin/forms/edit.php?form_id=<?php echo $this->_tpl_vars['form_id']; ?>
&page=main"><?php echo smarty_function_display_form_name(array('form_id' => $this->_tpl_vars['form_id']), $this);?>
</a></td>
        <td class="pad_left"><?php echo $this->_tpl_vars['L']['phrase_all_views']; ?>
</td>
      </tr>
    <?php endforeach; endif; unset($_from); ?>
    </table>

  <?php else: ?>

    <p>
      <?php echo $this->_tpl_vars['L']['text_details_desc']; ?>

    </p>

    <table cellspacing="1" cellpadding="0" class="list_table">
    <tr>
      <th colspan="2"><?php echo $this->_tpl_vars['LANG']['phrase_main_settings']; ?>
</th>
    </tr>
    <tr<?php if (((is_array($_tmp='account_status')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['changes']) : in_array($_tmp, $this->_tpl_vars['changes']))): ?> class="changed"<?php endif; ?>>
      <td width="180" class="pad_left"><?php echo $this->_tpl_vars['L']['phrase_account_status']; ?>
</td>
      <td class="pad_left"><?php echo $this->_tpl_vars['change_info']['account_info']['account_status']; ?>
</td>
    </tr>
    <tr<?php if (((is_array($_tmp='first_name')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['changes']) : in_array($_tmp, $this->_tpl_vars['changes']))): ?> class="changed"<?php endif; ?>>
      <td width="180" class="pad_left"><?php echo $this->_tpl_vars['LANG']['phrase_first_name']; ?>
</td>
      <td class="pad_left"><?php echo $this->_tpl_vars['change_info']['account_info']['first_name']; ?>
</td>
    </tr>
    <tr<?php if (((is_array($_tmp='last_name')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['changes']) : in_array($_tmp, $this->_tpl_vars['changes']))): ?> class="changed"<?php endif; ?>>
      <td class="pad_left"><?php echo $this->_tpl_vars['LANG']['phrase_last_name']; ?>
</td>
      <td class="pad_left"><?php echo $this->_tpl_vars['change_info']['account_info']['last_name']; ?>
</td>
    </tr>
    <tr<?php if (((is_array($_tmp='email')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['changes']) : in_array($_tmp, $this->_tpl_vars['changes']))): ?> class="changed"<?php endif; ?>>
      <td width="180" class="pad_left"><?php echo $this->_tpl_vars['LANG']['word_email']; ?>
</td>
      <td class="pad_left"><?php echo $this->_tpl_vars['change_info']['account_info']['email']; ?>
</td>
    </tr>
    <tr<?php if (((is_array($_tmp='username')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['changes']) : in_array($_tmp, $this->_tpl_vars['changes']))): ?> class="changed"<?php endif; ?>>
      <td width="180" class="pad_left">Username</td>
      <td class="pad_left"><?php echo $this->_tpl_vars['change_info']['account_info']['username']; ?>
</td>
    </tr>
    <tr<?php if (((is_array($_tmp='password')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['changes']) : in_array($_tmp, $this->_tpl_vars['changes']))): ?> class="changed"<?php endif; ?>>
      <td width="180" class="pad_left"><?php echo $this->_tpl_vars['LANG']['word_password']; ?>
</td>
      <td class="pad_left"><?php echo $this->_tpl_vars['change_info']['account_info']['password']; ?>
</td>
    </tr>
    <tr<?php if (((is_array($_tmp='ui_language')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['changes']) : in_array($_tmp, $this->_tpl_vars['changes']))): ?> class="changed"<?php endif; ?>>
      <td width="180" class="pad_left"><?php echo $this->_tpl_vars['LANG']['word_language']; ?>
</td>
      <td class="pad_left"><?php echo $this->_tpl_vars['change_info']['account_info']['ui_language']; ?>
</td>
    </tr>
    <tr<?php if (((is_array($_tmp='timezone_offset')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['changes']) : in_array($_tmp, $this->_tpl_vars['changes']))): ?> class="changed"<?php endif; ?>>
      <td width="180" class="pad_left"><?php echo $this->_tpl_vars['LANG']['phrase_system_time_offset']; ?>
</td>
      <td class="pad_left"><?php echo $this->_tpl_vars['change_info']['account_info']['timezone_offset']; ?>
</td>
    </tr>
    <tr<?php if (((is_array($_tmp='sessions_timeout')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['changes']) : in_array($_tmp, $this->_tpl_vars['changes']))): ?> class="changed"<?php endif; ?>>
      <td width="180" class="pad_left"><?php echo $this->_tpl_vars['LANG']['phrase_sessions_timeout']; ?>
</td>
      <td class="pad_left"><?php echo $this->_tpl_vars['change_info']['account_info']['sessions_timeout']; ?>
</td>
    </tr>
    <tr<?php if (((is_array($_tmp='date_format')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['changes']) : in_array($_tmp, $this->_tpl_vars['changes']))): ?> class="changed"<?php endif; ?>>
      <td width="180" class="pad_left"><?php echo $this->_tpl_vars['LANG']['phrase_date_format']; ?>
</td>
      <td class="pad_left"><?php echo $this->_tpl_vars['change_info']['account_info']['date_format']; ?>
</td>
    </tr>
    <tr<?php if (((is_array($_tmp='login_page')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['changes']) : in_array($_tmp, $this->_tpl_vars['changes']))): ?> class="changed"<?php endif; ?>>
      <td width="180" class="pad_left"><?php echo $this->_tpl_vars['LANG']['phrase_login_page']; ?>
</td>
      <td class="pad_left"><?php echo $this->_tpl_vars['change_info']['account_info']['login_page']; ?>
</td>
    </tr>
    <tr<?php if (((is_array($_tmp='logout_url')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['changes']) : in_array($_tmp, $this->_tpl_vars['changes']))): ?> class="changed"<?php endif; ?>>
      <td width="180" class="pad_left"><?php echo $this->_tpl_vars['LANG']['phrase_logout_url']; ?>
</td>
      <td class="pad_left"><?php echo $this->_tpl_vars['change_info']['account_info']['logout_url']; ?>
</td>
    </tr>
    <tr<?php if (((is_array($_tmp='theme')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['changes']) : in_array($_tmp, $this->_tpl_vars['changes']))): ?> class="changed"<?php endif; ?>>
      <td width="180" class="pad_left"><?php echo $this->_tpl_vars['LANG']['word_theme']; ?>
</td>
      <td class="pad_left"><?php echo $this->_tpl_vars['change_info']['account_info']['theme']; ?>
</td>
    </tr>
    <tr<?php if (((is_array($_tmp='swatch')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['changes']) : in_array($_tmp, $this->_tpl_vars['changes']))): ?> class="changed"<?php endif; ?>>
      <td width="180" class="pad_left"><?php echo $this->_tpl_vars['L']['word_swatch']; ?>
</td>
      <td class="pad_left"><?php echo $this->_tpl_vars['change_info']['account_info']['swatch']; ?>
</td>
    </tr>
    <tr<?php if (((is_array($_tmp='menu_id')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['changes']) : in_array($_tmp, $this->_tpl_vars['changes']))): ?> class="changed"<?php endif; ?>>
      <td width="180" class="pad_left"><?php echo $this->_tpl_vars['LANG']['word_menu']; ?>
</td>
      <td class="pad_left"><?php echo $this->_tpl_vars['change_info']['account_info']['menu_id']; ?>
</td>
    </tr>
    </table>

    <br />

    <?php if ($this->_tpl_vars['changed_settings']): ?>
      <table cellspacing="1" cellpadding="0" class="list_table">
      <tr>
        <th colspan="2"><?php echo $this->_tpl_vars['L']['phrase_other_changed_settings']; ?>
</th>
      </tr>
      <?php $_from = $this->_tpl_vars['changed_settings']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['setting_name'] => $this->_tpl_vars['setting_value']):
?>
      <tr class="changed">
        <td width="180">
          <?php if (((is_array($_tmp=$this->_tpl_vars['setting_name'])) ? $this->_run_mod_handler('array_key_exists', true, $_tmp, $this->_tpl_vars['settings_labels']) : array_key_exists($_tmp, $this->_tpl_vars['settings_labels']))): ?>
            <?php echo $this->_tpl_vars['settings_labels'][$this->_tpl_vars['setting_name']]; ?>

          <?php else: ?>
            <?php echo $this->_tpl_vars['setting_name']; ?>

          <?php endif; ?>
        </td>
        <td><?php echo $this->_tpl_vars['setting_value']; ?>
</td>
      </tr>
      <?php endforeach; endif; unset($_from); ?>
      </table>
    <?php endif; ?>

  <?php endif; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'modules_footer.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>