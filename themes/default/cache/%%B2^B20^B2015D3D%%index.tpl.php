<?php /* Smarty version 2.6.18, created on 2016-04-12 13:00:49
         compiled from C:%5Cxampp%5Chtdocs%5Clogbook/modules/client_audit/templates/index.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'C:\\xampp\\htdocs\\logbook/modules/client_audit/templates/index.tpl', 32, false),array('modifier', 'in_array', 'C:\\xampp\\htdocs\\logbook/modules/client_audit/templates/index.tpl', 48, false),array('modifier', 'escape', 'C:\\xampp\\htdocs\\logbook/modules/client_audit/templates/index.tpl', 114, false),array('modifier', 'custom_format_date', 'C:\\xampp\\htdocs\\logbook/modules/client_audit/templates/index.tpl', 166, false),array('modifier', 'upper', 'C:\\xampp\\htdocs\\logbook/modules/client_audit/templates/index.tpl', 198, false),array('function', 'display_account_name', 'C:\\xampp\\htdocs\\logbook/modules/client_audit/templates/index.tpl', 170, false),array('function', 'display_deleted_account_name', 'C:\\xampp\\htdocs\\logbook/modules/client_audit/templates/index.tpl', 172, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'modules_header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

  <table cellpadding="0" cellspacing="0">
  <tr>
    <td width="45"><a href="index.php"><img src="images/icon_client_audit.gif" border="0" width="34" height="34" /></a></td>
    <td class="title">
      <a href="../../admin/modules"><?php echo $this->_tpl_vars['LANG']['word_modules']; ?>
</a>
      <span class="joiner">&raquo;</span>
      <?php echo $this->_tpl_vars['L']['module_name']; ?>

    </td>
  </tr>
  </table>

  <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'messages.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

  <?php if ($this->_tpl_vars['total_count'] == 0): ?>
    <div><?php echo $this->_tpl_vars['L']['notify_no_activity']; ?>
</div>
  <?php else: ?>

    <form action="<?php echo $this->_tpl_vars['same_page']; ?>
" method="post">
      <table cellspacing="1" cellpadding="0" id="search_table" class="margin_bottom" width="100%">
        <tr>
          <td width="120"><?php echo $this->_tpl_vars['L']['phrase_client_account']; ?>
</td>
          <td colspan="2">
            <select name="client_id">
              <option value="" <?php if ($this->_tpl_vars['search_criteria']['client_id'] == ""): ?>selected<?php endif; ?>><?php echo $this->_tpl_vars['LANG']['phrase_all_clients']; ?>
</option>
              <optgroup label="<?php echo $this->_tpl_vars['L']['phrase_current_clients']; ?>
">
              <?php $_from = $this->_tpl_vars['clients']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['row'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['row']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['client']):
        $this->_foreach['row']['iteration']++;
?>
                <option value="<?php echo $this->_tpl_vars['client']['account_id']; ?>
" <?php if ($this->_tpl_vars['search_criteria']['client_id'] == $this->_tpl_vars['client']['account_id']): ?>selected<?php endif; ?>><?php echo $this->_tpl_vars['client']['last_name']; ?>
, <?php echo $this->_tpl_vars['client']['first_name']; ?>
</option>
              <?php endforeach; endif; unset($_from); ?>
              </optgroup>
              <?php if (count($this->_tpl_vars['deleted_clients']) > 0): ?>
                <optgroup label="<?php echo $this->_tpl_vars['L']['phrase_deleted_clients']; ?>
">
                <?php $_from = $this->_tpl_vars['deleted_clients']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['row'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['row']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['client']):
        $this->_foreach['row']['iteration']++;
?>
                  <option value="<?php echo $this->_tpl_vars['client']['account_id']; ?>
" <?php if ($this->_tpl_vars['search_criteria']['client_id'] == $this->_tpl_vars['client']['account_id']): ?>selected<?php endif; ?>><?php echo $this->_tpl_vars['client']['last_name']; ?>
, <?php echo $this->_tpl_vars['client']['first_name']; ?>
</option>
                <?php endforeach; endif; unset($_from); ?>
                </optgroup>
              <?php endif; ?>
            </select>
          </td>
        </tr>
        <tr>
          <td><?php echo $this->_tpl_vars['L']['word_events']; ?>
</td>
          <td valign="top">
            <ul>
              <li>
                <input type="checkbox" name="change_types[]" id="ct1" value="login"
                  <?php if (((is_array($_tmp='login')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['search_criteria']['change_types']) : in_array($_tmp, $this->_tpl_vars['search_criteria']['change_types']))): ?>checked<?php endif; ?> />
                  <label for="ct1"><?php echo $this->_tpl_vars['LANG']['word_login']; ?>
</label>
              </li>
              <li>
                <input type="checkbox" name="change_types[]" id="ct2" value="logout"
                  <?php if (((is_array($_tmp='logout')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['search_criteria']['change_types']) : in_array($_tmp, $this->_tpl_vars['search_criteria']['change_types']))): ?>checked<?php endif; ?> />
                  <label for="ct2"><?php echo $this->_tpl_vars['LANG']['word_logout']; ?>
</label>
              </li>
              <li>
                <input type="checkbox" name="change_types[]" id="ct3" value="account_created"
                  <?php if (((is_array($_tmp='account_created')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['search_criteria']['change_types']) : in_array($_tmp, $this->_tpl_vars['search_criteria']['change_types']))): ?>checked<?php endif; ?> />
                  <label for="ct3"><?php echo $this->_tpl_vars['L']['phrase_account_created']; ?>
</label>
              </li>
              <li>
                <input type="checkbox" name="change_types[]" id="ct4" value="account_deleted"
                  <?php if (((is_array($_tmp='account_deleted')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['search_criteria']['change_types']) : in_array($_tmp, $this->_tpl_vars['search_criteria']['change_types']))): ?>checked<?php endif; ?> />
                  <label for="ct4"><?php echo $this->_tpl_vars['L']['phrase_account_deleted']; ?>
</label>
              </li>
            </ul>
          </td>
          <td valign="top">
            <ul>
              <li>
                <input type="checkbox" name="change_types[]" id="ct5" value="permissions"
                  <?php if (((is_array($_tmp='permissions')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['search_criteria']['change_types']) : in_array($_tmp, $this->_tpl_vars['search_criteria']['change_types']))): ?>checked<?php endif; ?> />
                  <label for="ct5"><?php echo $this->_tpl_vars['L']['phrase_permissions_updated']; ?>
</label>
              </li>
              <li>
                <input type="checkbox" name="change_types[]" id="ct6" value="client_update"
                  <?php if (((is_array($_tmp='client_update')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['search_criteria']['change_types']) : in_array($_tmp, $this->_tpl_vars['search_criteria']['change_types']))): ?>checked<?php endif; ?> />
                  <label for="ct6"><?php echo $this->_tpl_vars['L']['phrase_account_updated_by_client']; ?>
</label>
              </li>
              <li>
                <input type="checkbox" name="change_types[]" id="ct7" value="admin_update"
                  <?php if (((is_array($_tmp='admin_update')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['search_criteria']['change_types']) : in_array($_tmp, $this->_tpl_vars['search_criteria']['change_types']))): ?>checked<?php endif; ?> />
                  <label for="ct7"><?php echo $this->_tpl_vars['L']['phrase_account_updated_by_admin']; ?>
</label>
              </li>
              <li>
                <input type="checkbox" name="change_types[]" id="ct8" value="account_disabled_from_failed_logins"
                  <?php if (((is_array($_tmp='account_disabled_from_failed_logins')) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['search_criteria']['change_types']) : in_array($_tmp, $this->_tpl_vars['search_criteria']['change_types']))): ?>checked<?php endif; ?> />
                  <label for="ct8"><?php echo $this->_tpl_vars['L']['phrase_account_disabled_failed_logins']; ?>
</label>
              </li>
            </ul>
          </td>
        </tr>
        <tr>
          <td valign="top"><?php echo $this->_tpl_vars['L']['word_dates']; ?>
</td>
          <td colspan="3">
            <table cellspacing="0" cellpadding="0">
            <tr>
              <td class="pad_right">
                <input type="radio" name="date_range" id="dr1" value="all" <?php if ($this->_tpl_vars['search_criteria']['date_range'] == 'all'): ?>checked<?php endif; ?>
                  onclick="page_ns.selectDateType(this.value)" />
                  <label for="dr1"><?php echo $this->_tpl_vars['L']['phrase_all_dates']; ?>
</label>
              </td>
              <td width="80" class="pad_right">
                <input type="radio" name="date_range" id="dr2" value="range" <?php if ($this->_tpl_vars['search_criteria']['date_range'] == 'range'): ?>checked<?php endif; ?>
                  onclick="page_ns.selectDateType(this.value)" />
                  <label for="dr2"><?php echo $this->_tpl_vars['L']['word_range']; ?>
</label>
              </td>
              <td>
                <table cellspacing="0" cellpadding="0">
                <tr>
                  <td class="pad_right"><?php echo $this->_tpl_vars['LANG']['word_from_c']; ?>
</td>
                  <td class="pad_right">
                    <input type="text" name="date_from" id="date_from" class="datepicker" style="width: 80px;"
                      value="<?php echo ((is_array($_tmp=$this->_tpl_vars['search_criteria']['date_from'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" <?php if ($this->_tpl_vars['search_criteria']['date_range'] == 'all'): ?>disabled<?php endif; ?> />
                  </td>
                  <td class="pad_right"><?php echo $this->_tpl_vars['L']['word_to_c']; ?>
</td>
                  <td>
                    <input type="text" name="date_to" id="date_to" class="datepicker" style="width: 80px;"
                      value="<?php echo ((is_array($_tmp=$this->_tpl_vars['search_criteria']['date_to'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" <?php if ($this->_tpl_vars['search_criteria']['date_range'] == 'all'): ?>disabled<?php endif; ?> />
                  </td>
                </tr>
                </table>
              </td>
            </tr>
            </table>
            <div class="vertical_pad"> </div>
          </td>
        </tr>
        <tr>
          <td colspan="3" align="right">
            <input type="submit" name="search_forms" value="<?php echo $this->_tpl_vars['LANG']['word_search']; ?>
" />
            <input type="button" name="reset" onclick="window.location='<?php echo $this->_tpl_vars['same_page']; ?>
?reset=1'"
              <?php if ($this->_tpl_vars['num_search_results'] < $this->_tpl_vars['total_count']): ?>
                value="<?php echo $this->_tpl_vars['LANG']['phrase_show_all']; ?>
 (<?php echo $this->_tpl_vars['total_count']; ?>
)" class="bold"
              <?php else: ?>
                value="<?php echo $this->_tpl_vars['LANG']['phrase_show_all']; ?>
" class="light_grey" disabled
              <?php endif; ?> />
          </td>
        </tr>
      </table>
    </form>

    <?php if ($this->_tpl_vars['num_search_results'] == 0): ?>
      <?php $this->assign('g_message', $this->_tpl_vars['L']['notify_no_results']); ?>
      <?php $this->assign('g_success', false); ?>
      <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'messages.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
    <?php else: ?>

      <?php echo $this->_tpl_vars['pagination']; ?>


      <form action="<?php echo $this->_tpl_vars['same_page']; ?>
" method="post" id="client_audit_form">
        <input type="hidden" name="delete_all" id="delete_all" value="" />

        <table class="list_table" cellpadding="1" cellspacing="1" border="0" width="650">
        <tr>
          <th width="30"><input type="checkbox" id="toggle" /></th>
          <th><?php echo $this->_tpl_vars['LANG']['word_date']; ?>
</th>
          <th><?php echo $this->_tpl_vars['LANG']['word_client']; ?>
</th>
          <th><?php echo $this->_tpl_vars['L']['word_event']; ?>
</th>
          <th width="80"><?php echo $this->_tpl_vars['L']['word_details']; ?>
</th>
        </tr>
        <?php $_from = $this->_tpl_vars['search_results']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['row']):
?>
          <tr>
            <td align="center"><input type="checkbox" name="change_ids[]" class="change_row" value="<?php echo $this->_tpl_vars['row']['change_id']; ?>
" /></td>
            <td>
              <?php echo ((is_array($_tmp=$this->_tpl_vars['row']['change_date'])) ? $this->_run_mod_handler('custom_format_date', true, $_tmp, $this->_tpl_vars['SESSION']['account']['timezone_offset'], $this->_tpl_vars['SESSION']['account']['date_format']) : smarty_modifier_custom_format_date($_tmp, $this->_tpl_vars['SESSION']['account']['timezone_offset'], $this->_tpl_vars['SESSION']['account']['date_format'])); ?>

            </td>
            <td>
              <?php if ($this->_tpl_vars['row']['account_exists']): ?>
                <a href="../../admin/clients/edit.php?client_id=<?php echo $this->_tpl_vars['row']['account_id']; ?>
"><?php echo smarty_function_display_account_name(array('account_id' => $this->_tpl_vars['row']['account_id'],'format' => 'last_first'), $this);?>
</a>
              <?php else: ?>
                <span class="light_grey"><?php echo smarty_function_display_deleted_account_name(array('account_id' => $this->_tpl_vars['row']['account_id'],'format' => 'last_first'), $this);?>
</span>
              <?php endif; ?>
            </td>
            <td>
              <?php if ($this->_tpl_vars['row']['change_type'] == 'login'): ?>
                <span class="ct_login"><?php echo $this->_tpl_vars['LANG']['word_login']; ?>
</span>
              <?php elseif ($this->_tpl_vars['row']['change_type'] == 'logout'): ?>
                <span class="ct_logout"><?php echo $this->_tpl_vars['LANG']['word_logout']; ?>
</span>
              <?php elseif ($this->_tpl_vars['row']['change_type'] == 'permissions'): ?>
                <span class="ct_permissions"><?php echo $this->_tpl_vars['L']['phrase_permissions_updated']; ?>
</span>
              <?php elseif ($this->_tpl_vars['row']['change_type'] == 'admin_update'): ?>
                <span class="ct_admin_update"><?php echo $this->_tpl_vars['L']['phrase_updated_by_admin']; ?>
</span>
              <?php elseif ($this->_tpl_vars['row']['change_type'] == 'client_update'): ?>
                <span class="ct_client_update"><?php echo $this->_tpl_vars['L']['phrase_updated_by_client']; ?>
</span>
              <?php elseif ($this->_tpl_vars['row']['change_type'] == 'account_created'): ?>
                <span class="ct_account_created"><?php echo $this->_tpl_vars['L']['phrase_account_created']; ?>
</span>
              <?php elseif ($this->_tpl_vars['row']['change_type'] == 'account_deleted'): ?>
                <span class="ct_account_deleted"><?php echo $this->_tpl_vars['L']['phrase_account_deleted']; ?>
</span>
              <?php elseif ($this->_tpl_vars['row']['change_type'] == 'account_disabled_from_failed_logins'): ?>
                <span class="ct_account_disabled_from_failed_logins"><?php echo $this->_tpl_vars['L']['phrase_account_disabled_failed_logins']; ?>
</span>
              <?php endif; ?>
            </td>
            <td align="center">
              <?php if ($this->_tpl_vars['row']['change_type'] == 'login' || $this->_tpl_vars['row']['change_type'] == 'logout'): ?>
                <span class="light_grey">&#8212;</span>
              <?php else: ?>
                <a href="details.php?change_id=<?php echo $this->_tpl_vars['row']['change_id']; ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['L']['word_details'])) ? $this->_run_mod_handler('upper', true, $_tmp) : smarty_modifier_upper($_tmp)); ?>
</a>
              <?php endif; ?>
            </td>
          </tr>
        <?php endforeach; endif; unset($_from); ?>
        </table>

        <p>
          <input type="button" id="delete_all_button" value="<?php echo $this->_tpl_vars['L']['phrase_delete_all_results']; ?>
" style="float: right"/>
          <input type="button" class="delete_selected" value="<?php echo $this->_tpl_vars['L']['phrase_delete_selected_rows']; ?>
" />
        </p>

      </form>

    <?php endif; ?>
  <?php endif; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'modules_footer.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>