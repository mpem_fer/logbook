<?php /* Smarty version 2.6.18, created on 2016-04-14 05:56:53
         compiled from C:%5Cxampp%5Chtdocs%5Clogbook/modules/data_visualization/templates/activity_charts/settings.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'upper', 'C:\\xampp\\htdocs\\logbook/modules/data_visualization/templates/activity_charts/settings.tpl', 30, false),array('function', 'activity_chart_date_range', 'C:\\xampp\\htdocs\\logbook/modules/data_visualization/templates/activity_charts/settings.tpl', 36, false),array('function', 'colour_dropdown', 'C:\\xampp\\htdocs\\logbook/modules/data_visualization/templates/activity_charts/settings.tpl', 67, false),array('function', 'line_width_dropdown', 'C:\\xampp\\htdocs\\logbook/modules/data_visualization/templates/activity_charts/settings.tpl', 73, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'modules_header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

  <table cellpadding="0" cellspacing="0">
  <tr>
    <td width="45"><a href="../index.php"><img src="../images/icon_visualization.png" border="0" width="34" height="34" /></a></td>
    <td class="title">
      <a href="../../../admin/modules"><?php echo $this->_tpl_vars['LANG']['word_modules']; ?>
</a>
      <span class="joiner">&raquo;</span>
      <a href="../"><?php echo $this->_tpl_vars['L']['module_name']; ?>
</a>
      <span class="joiner">&raquo;</span>
      <?php echo $this->_tpl_vars['L']['phrase_default_activity_chart_settings']; ?>

    </td>
  </tr>
  </table>

  <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'messages.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

  <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "../../modules/data_visualization/no_internet_connection.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

  <div class="margin_bottom_large">
    <?php echo $this->_tpl_vars['L']['text_default_activity_chart_settings_page']; ?>

  </div>

  <form method="post" action="<?php echo $this->_tpl_vars['same_page']; ?>
">

    <table cellspacing="0" cellpadding="0" width="100%" class="margin_bottom_large">
    <tr>
      <td valign="top">

        <div class="subtitle underline margin_bottom_large"><?php echo ((is_array($_tmp=$this->_tpl_vars['L']['phrase_default_settings'])) ? $this->_run_mod_handler('upper', true, $_tmp) : smarty_modifier_upper($_tmp)); ?>
</div>

        <table cellspacing="0" cellpadding="1" class="list_table">
        <tr>
          <td class="pad_left_small"><?php echo $this->_tpl_vars['L']['phrase_date_range_to_display']; ?>
</td>
          <td>
            <?php echo smarty_function_activity_chart_date_range(array('name_id' => 'date_range','default' => $this->_tpl_vars['module_settings']['activity_chart_date_range']), $this);?>

          </td>
        </tr>
        <tr>
          <td class="pad_left_small"><?php echo $this->_tpl_vars['L']['phrase_group_submission_count_by']; ?>
</td>
          <td>
            <input type="radio" name="submission_count_group" id="scd1" value="month"
              <?php if ($this->_tpl_vars['module_settings']['activity_chart_submission_count_group'] == 'month'): ?>checked<?php endif; ?> />
              <label for="scd1"><?php echo $this->_tpl_vars['L']['word_month']; ?>
</label>
            <input type="radio" name="submission_count_group" id="scd3" value="day"
              <?php if ($this->_tpl_vars['module_settings']['activity_chart_submission_count_group'] == 'day'): ?>checked<?php endif; ?> />
              <label for="scd3"><?php echo $this->_tpl_vars['L']['word_day']; ?>
</label>
          </td>
        </tr>
        <tr>
          <td class="pad_left_small"><?php echo $this->_tpl_vars['L']['phrase_chart_type']; ?>
</td>
          <td>
            <input type="radio" name="chart_type" id="lc1" value="line_chart"
              <?php if ($this->_tpl_vars['module_settings']['activity_chart_default_chart_type'] == 'line_chart'): ?>checked<?php endif; ?> />
              <label for="lc1"><?php echo $this->_tpl_vars['L']['phrase_line_chart']; ?>
</label>
            <input type="radio" name="chart_type" id="lc2" value="area_chart"
              <?php if ($this->_tpl_vars['module_settings']['activity_chart_default_chart_type'] == 'area_chart'): ?>checked<?php endif; ?> />
              <label for="lc2"><?php echo $this->_tpl_vars['L']['phrase_area_chart']; ?>
</label>
            <input type="radio" name="chart_type" id="lc3" value="column_chart"
              <?php if ($this->_tpl_vars['module_settings']['activity_chart_default_chart_type'] == 'column_chart'): ?>checked<?php endif; ?> />
              <label for="lc3"><?php echo $this->_tpl_vars['L']['phrase_column_chart']; ?>
</label>
          </td>
        </tr>
        <tr>
          <td class="pad_left_small"><?php echo $this->_tpl_vars['L']['word_colour']; ?>
</td>
          <td>
            <?php echo smarty_function_colour_dropdown(array('name_id' => 'colour','default' => $this->_tpl_vars['module_settings']['activity_chart_colour']), $this);?>

          </td>
        </tr>
        <tr>
          <td class="pad_left_small"><?php echo $this->_tpl_vars['L']['phrase_line_width']; ?>
</td>
          <td>
            <?php echo smarty_function_line_width_dropdown(array('name_id' => 'line_width','default' => $this->_tpl_vars['module_settings']['activity_chart_line_width']), $this);?>
 px
            <div class="hint">This is for Line and Area charts only.</div>
          </td>
        </tr>
        </table>
      </td>
      <td width="250" valign="top">
        <div id="thumb_chart"></div>
      </td>
    </tr>
    </table>

    <div class="subtitle underline margin_bottom_large"><?php echo ((is_array($_tmp=$this->_tpl_vars['L']['phrase_full_size_example'])) ? $this->_run_mod_handler('upper', true, $_tmp) : smarty_modifier_upper($_tmp)); ?>
</div>
    <div id="full_size_chart"></div>

    <p>
      <input type="submit" name="update" value="<?php echo $this->_tpl_vars['LANG']['word_update']; ?>
" />
    </p>

  </form>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'modules_footer.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>