<?php /* Smarty version 2.6.18, created on 2016-04-20 10:57:48
         compiled from C:%5Cxampp%5Chtdocs%5Clogbook/modules/submission_history/templates/index.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'upper', 'C:\\xampp\\htdocs\\logbook/modules/submission_history/templates/index.tpl', 42, false),array('modifier', 'in_array', 'C:\\xampp\\htdocs\\logbook/modules/submission_history/templates/index.tpl', 51, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'modules_header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

  <table cellpadding="0" cellspacing="0">
  <tr>
    <td width="45"><a href="index.php"><img src="images/icon_submission_history.gif" border="0" width="34" height="34" /></a></td>
    <td class="title">
      <a href="../../admin/modules"><?php echo $this->_tpl_vars['LANG']['word_modules']; ?>
</a>
      <span class="joiner">&raquo;</span>
      <?php echo $this->_tpl_vars['L']['module_name']; ?>

    </td>
  </tr>
  </table>

  <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'messages.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

  <?php if ($this->_tpl_vars['module_settings']['history_tables_created'] == 'no'): ?>

    <form action="<?php echo $this->_tpl_vars['same_page']; ?>
" method="post" id="create_history_table_form">
      <div class="margin_bottom_large">
        <?php echo $this->_tpl_vars['L']['text_module_intro']; ?>

      </div>
      <p>
        <input type="button" name="create_history_table" id="create_history_table"
          value="<?php echo $this->_tpl_vars['L']['phrase_create_history_tables']; ?>
" onclick="page_ns.create_history_tables()" />
      </p>
    </form>

  <?php else: ?>

    <form action="<?php echo $this->_tpl_vars['same_page']; ?>
" method="post">

      <div class="margin_bottom_large">
        <?php echo $this->_tpl_vars['L']['text_tracking_table']; ?>

      </div>
      <table cellspacing="1" cellpadding="0" class="list_table check_areas">
      <tr>
        <th> </th>
        <th><?php echo $this->_tpl_vars['LANG']['word_form']; ?>
</th>
        <th width="100"><?php echo $this->_tpl_vars['L']['phrase_track_activity']; ?>
</th>
        <th><?php echo $this->_tpl_vars['L']['phrase_table_size_kb']; ?>
</th>
        <th><?php echo $this->_tpl_vars['L']['phrase_num_rows']; ?>
</th>
        <th><?php echo ((is_array($_tmp=$this->_tpl_vars['L']['word_undelete'])) ? $this->_run_mod_handler('upper', true, $_tmp) : smarty_modifier_upper($_tmp)); ?>
</th>
        <th width="80"><?php echo $this->_tpl_vars['L']['phrase_clear_logs']; ?>
</th>
      </tr>
      <?php $_from = $this->_tpl_vars['forms']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['form']):
?>
      <tr>
        <td class="pad_left_small light_grey"><?php echo $this->_tpl_vars['form']['form_id']; ?>
</td>
        <td class="pad_left_small"><a href="../../admin/forms/submissions.php?form_id=<?php echo $this->_tpl_vars['form']['form_id']; ?>
"><?php echo $this->_tpl_vars['form']['form_name']; ?>
</a></td>
        <td align="center" class="check_area">
          <input type="checkbox" name="tracked_form_ids[]" value="<?php echo $this->_tpl_vars['form']['form_id']; ?>
"
            <?php if (((is_array($_tmp=$this->_tpl_vars['form']['form_id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['configured_form_ids']) : in_array($_tmp, $this->_tpl_vars['configured_form_ids']))): ?>checked<?php endif; ?> />
        </td>
        <td align="center"><?php echo $this->_tpl_vars['form']['history_table_size']; ?>
</td>
        <td align="center"><?php echo $this->_tpl_vars['form']['history_table_rows']; ?>
</td>
        <td align="center">
          <?php if ($this->_tpl_vars['form']['num_deleted_submissions'] == 0): ?>
            <span class="light_grey"><?php echo ((is_array($_tmp=$this->_tpl_vars['L']['word_undelete'])) ? $this->_run_mod_handler('upper', true, $_tmp) : smarty_modifier_upper($_tmp)); ?>
</span>
          <?php else: ?>
            <a href="undelete.php?form_id=<?php echo $this->_tpl_vars['form']['form_id']; ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['L']['word_undelete'])) ? $this->_run_mod_handler('upper', true, $_tmp) : smarty_modifier_upper($_tmp)); ?>
</a> (<?php echo $this->_tpl_vars['form']['num_deleted_submissions']; ?>
)
          <?php endif; ?>
        </td>
        <td><input type="button" value="<?php echo $this->_tpl_vars['L']['phrase_clear_logs']; ?>
" onclick="page_ns.clear_logs(<?php echo $this->_tpl_vars['form']['form_id']; ?>
)"/></td>
        </tr>
      <?php endforeach; endif; unset($_from); ?>
      </table>

      <p>
        <input type="submit" name="update_activity_tracking" value="<?php echo $this->_tpl_vars['L']['phrase_update_form_activity_tracking']; ?>
" />
        <input type="submit" name="clear_all_logs" value="<?php echo $this->_tpl_vars['L']['phrase_clear_all_logs']; ?>
" class="burgundy" />
      </p>
    </form>

  <?php endif; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'modules_footer.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>