<?php /* Smarty version 2.6.18, created on 2016-06-09 04:10:14
         compiled from C:%5Cxampp%5Chtdocs%5Clogbook/modules/submission_history/templates/list_history.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'upper', 'C:\\xampp\\htdocs\\logbook/modules/submission_history/templates/list_history.tpl', 19, false),)), $this); ?>
  <!-- search -->
  <div id="sh__pagination"><?php echo $this->_tpl_vars['pagination']; ?>
</div>

  <ul>
    <li>
      <div>
        <div class="sh__view_change"></div>
        <div class="sh__date bold"><?php echo $this->_tpl_vars['L']['phrase_change_date']; ?>
</div>
        <div class="sh__change_type bold"><?php echo $this->_tpl_vars['L']['phrase_change_type']; ?>
</div>
        <div class="sh__changed_by bold"><?php echo $this->_tpl_vars['L']['phrase_changed_by']; ?>
</div>
        <div class="sh__changes bold"><?php echo $this->_tpl_vars['L']['phrase_changed_fields']; ?>
</div>
      </div>
      <div style="clear: both"></div>
    </li>
  <?php $_from = $this->_tpl_vars['results']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
    <li>
      <div>
        <div class="sh__view_change">
          <a href="javascript:sh.view_history_changes(<?php echo $this->_tpl_vars['item']['sh___history_id']; ?>
);"><?php echo ((is_array($_tmp=$this->_tpl_vars['LANG']['word_view'])) ? $this->_run_mod_handler('upper', true, $_tmp) : smarty_modifier_upper($_tmp)); ?>
</a>
        </div>
        <div class="sh__date"><?php echo $this->_tpl_vars['item']['sh___change_date']; ?>
</div>
        <div class="sh__change_type">
          <?php if ($this->_tpl_vars['item']['sh___change_type'] == 'new'): ?>
            <span class="change_type_new"><?php echo $this->_tpl_vars['L']['word_new']; ?>
</span>
          <?php elseif ($this->_tpl_vars['item']['sh___change_type'] == 'update'): ?>
            <span class="change_type_update"><?php echo $this->_tpl_vars['L']['word_update']; ?>
</span>
          <?php elseif ($this->_tpl_vars['item']['sh___change_type'] == 'restore'): ?>
            <span class="change_type_restore"><?php echo $this->_tpl_vars['L']['word_restored']; ?>
</span>
          <?php elseif ($this->_tpl_vars['item']['sh___change_type'] == 'delete'): ?>
            <span class="change_type_delete"><?php echo $this->_tpl_vars['L']['word_deleted']; ?>
</span>
          <?php elseif ($this->_tpl_vars['item']['sh___change_type'] == 'original'): ?>
            <span class="change_type_original"><?php echo $this->_tpl_vars['L']['word_original']; ?>
</span>
          <?php elseif ($this->_tpl_vars['item']['sh___change_type'] == 'undelete'): ?>
            <span class="change_type_undelete"><?php echo $this->_tpl_vars['L']['word_undelete']; ?>
</span>
          <?php elseif ($this->_tpl_vars['item']['sh___change_type'] == 'submission'): ?>
            <span class="change_type_submission"><?php echo $this->_tpl_vars['L']['word_submission']; ?>
</span>
          <?php endif; ?>
        </div>
        <div class="sh__changed_by">
          <?php if ($this->_tpl_vars['item']['sh___change_account_id'] == 1): ?>
            <?php echo $this->_tpl_vars['client_info'][$this->_tpl_vars['item']['sh___change_account_id']]['first_name']; ?>
 <?php echo $this->_tpl_vars['client_info'][$this->_tpl_vars['item']['sh___change_account_id']]['last_name']; ?>

          <?php elseif ($this->_tpl_vars['item']['sh___change_type'] == 'submission'): ?>
            <?php echo $this->_tpl_vars['L']['phrase_submission_accounts_module']; ?>

          <?php else: ?>
            <a href="../clients/edit.php?client_id=<?php echo $this->_tpl_vars['item']['sh___change_account_id']; ?>
"><?php echo $this->_tpl_vars['client_info'][$this->_tpl_vars['item']['sh___change_account_id']]['first_name']; ?>
 <?php echo $this->_tpl_vars['client_info'][$this->_tpl_vars['item']['sh___change_account_id']]['last_name']; ?>
</a>
          <?php endif; ?>&nbsp;
        </div>
        <div class="sh__changes">
          <?php if ($this->_tpl_vars['item']['num_changed_fields'] == 0): ?>
            <span class="no_changed_fields">&#8212;</span>
          <?php else: ?>
            <select>
              <option><?php echo $this->_tpl_vars['L']['phrase_changed_fields_c']; ?>
 <?php echo $this->_tpl_vars['item']['num_changed_fields']; ?>
</option>
              <?php $_from = $this->_tpl_vars['item']['changed_fields']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['field_info']):
?>
                <option><?php echo $this->_tpl_vars['field_info']; ?>
</option>
              <?php endforeach; endif; unset($_from); ?>
            </select>
          <?php endif; ?>
        </div>
      </div>
      <div style="clear: both"></div>
    </li>
  <?php endforeach; endif; unset($_from); ?>
  </ul>

  <p>
    <input type="button" value="<?php echo $this->_tpl_vars['L']['phrase_clear_submission_log']; ?>
" onclick="sh.clear_submission_log()" />
  </p>