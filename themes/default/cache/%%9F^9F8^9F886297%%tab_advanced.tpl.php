<?php /* Smarty version 2.6.18, created on 2016-05-18 11:25:49
         compiled from C:%5Cxampp%5Chtdocs%5Clogbook/themes/default/../../modules/data_visualization/templates/field_charts/tab_advanced.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'upper', 'C:\\xampp\\htdocs\\logbook/themes/default/../../modules/data_visualization/templates/field_charts/tab_advanced.tpl', 3, false),array('modifier', 'escape', 'C:\\xampp\\htdocs\\logbook/themes/default/../../modules/data_visualization/templates/field_charts/tab_advanced.tpl', 70, false),array('function', 'menus_dropdown', 'C:\\xampp\\htdocs\\logbook/themes/default/../../modules/data_visualization/templates/field_charts/tab_advanced.tpl', 75, false),)), $this); ?>
  <input type="hidden" id="vis_id" value="<?php echo $this->_tpl_vars['vis_id']; ?>
" />

  <div class="subtitle underline margin_top_large"><?php echo ((is_array($_tmp=$this->_tpl_vars['L']['phrase_pages_module'])) ? $this->_run_mod_handler('upper', true, $_tmp) : smarty_modifier_upper($_tmp)); ?>
</div>

  <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "messages.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

  <?php if ($this->_tpl_vars['pages_module_available']): ?>
    <div class="margin_bottom_large">
      <?php echo $this->_tpl_vars['L']['text_pages_module_intro']; ?>

    </div>

    <div class="grey_box margin_bottom_large">
      <div class="margin_bottom bold"><?php echo $this->_tpl_vars['L']['phrase_smarty_pages']; ?>
</div>
      <input type="text" style="width: 99%" class="medium_grey"
        value="<?php echo '{'; ?>
template_hook location=&quot;data_visualization&quot; vis_id=<?php echo $this->_tpl_vars['vis_id']; ?>
 height=300 width=600<?php echo '}'; ?>
" />
    </div>

    <div class="grey_box margin_bottom_large">
      <div class="margin_bottom bold"><?php echo $this->_tpl_vars['L']['phrase_php_pages']; ?>
</div>
      <textarea class="medium_grey" style="width:99%; height: 100px">ft_include_module("data_visualization");
$width  = 600;
$height = 300;
dv_display_visualization(<?php echo $this->_tpl_vars['vis_id']; ?>
, $width, $height);</textarea>
    </div>

    <div>
      <input type="submit" value="<?php echo $this->_tpl_vars['L']['phrase_create_page_add_menu_item']; ?>
" id="add_to_menu" />
      <span class="light_grey">|</span> <a href="../../pages/"><?php echo $this->_tpl_vars['L']['phrase_goto_pages_module']; ?>
</a>
      <span class="light_grey">|</span> <a href="../../../admin/settings/index.php?page=menus"><?php echo $this->_tpl_vars['L']['phrase_goto_menus_page']; ?>
</a>
    </div>
    <br />

  <?php else: ?>
    <div class="notify">
      <div style="padding:6px">
        <?php echo $this->_tpl_vars['L']['notify_pages_module_not_installed']; ?>

      </div>
    </div>
  <?php endif; ?>

  <div class="subtitle underline margin_bottom_large margin_top_large"><?php echo ((is_array($_tmp=$this->_tpl_vars['L']['phrase_use_in_own_pages'])) ? $this->_run_mod_handler('upper', true, $_tmp) : smarty_modifier_upper($_tmp)); ?>
</div>

  <div class="margin_bottom_large">
    <?php echo $this->_tpl_vars['L']['text_use_in_pages_desc']; ?>

  </div>

  <textarea style="width:100%; height: 150px" class="medium_grey">&lt;?php
require_once('<?php echo $this->_tpl_vars['g_root_dir']; ?>
/global/library.php');
ft_include_module("data_visualization");
$width  = 600;
$height = 300;
dv_display_visualization(<?php echo $this->_tpl_vars['vis_id']; ?>
, $width, $height);
<?php echo '?>'; ?>
</textarea>

  <p>
    <input type="button" id="delete_visualization" value="<?php echo $this->_tpl_vars['L']['phrase_delete_visualization']; ?>
" class="burgundy right" />
  </p>
  <div class="clear"></div>

<div id="add_to_menu_dialog" class="hidden">
  <div class="margin_bottom_large">
    <?php echo $this->_tpl_vars['L']['text_create_page_desc']; ?>

  </div>

  <table width="100%">
  <tr>
    <td width="160" class="medium_grey"><?php echo $this->_tpl_vars['L']['phrase_page_menu_title']; ?>
</td>
    <td>
      <input type="text" style="width: 100%" id="page_title" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['vis_info']['vis_name'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" />
    </td>
  </tr>
  <tr>
    <td valign="top" class="medium_grey">Menu</td>
    <td><?php echo smarty_function_menus_dropdown(array('name_id' => 'menu_id'), $this);?>
</td>
  </tr>
  <tr>
    <td class="medium_grey"><?php echo $this->_tpl_vars['L']['word_position']; ?>
</td>
    <td>
      <div id="position_div" class="medium_grey"><?php echo $this->_tpl_vars['L']['phrase_please_select_menu']; ?>
</div>
    </td>
  </tr>
  <tr>
    <td class="medium_grey"><?php echo $this->_tpl_vars['L']['phrase_submenu_item']; ?>
</td>
    <td>
      <input type="radio" name="is_submenu" id="is1" value="yes" />
        <label for="is1"><?php echo $this->_tpl_vars['LANG']['word_yes']; ?>
</label>
      <input type="radio" name="is_submenu" id="is2" value="no" checked="checked" />
        <label for="is2"><?php echo $this->_tpl_vars['LANG']['word_no']; ?>
</label>
    </td>
  </tr>
  </table>

</div>