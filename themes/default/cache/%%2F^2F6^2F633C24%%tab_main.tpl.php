<?php /* Smarty version 2.6.18, created on 2016-04-12 13:01:22
         compiled from ../../modules/export_manager/templates/export_groups/tab_main.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'escape', '../../modules/export_manager/templates/export_groups/tab_main.tpl', 10, false),)), $this); ?>
  <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'messages.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

  <form action="<?php echo $this->_tpl_vars['samepage']; ?>
" method="post" onsubmit="return rsv.validate(this, rules)">
    <input type="hidden" name="export_group_id" value="<?php echo $this->_tpl_vars['export_group_info']['export_group_id']; ?>
" />

    <table cellspacing="1" cellpadding="2" border="0" width="100%" class="edit_export_group_table">
    <tr>
      <td width="130" class="medium_grey"><?php echo $this->_tpl_vars['L']['phrase_export_group_name']; ?>
</td>
      <td>
        <input type="text" name="group_name" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['export_group_info']['group_name'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" style="width:100%" maxlength="255" />
      </td>
    </tr>
    <tr>
      <td class="medium_grey"><?php echo $this->_tpl_vars['L']['word_visibility']; ?>
</td>
      <td>
        <input type="radio" name="visibility" value="show" id="st1" <?php if ($this->_tpl_vars['export_group_info']['visibility'] == 'show'): ?>checked<?php endif; ?> />
          <label for="st1" class="green"><?php echo $this->_tpl_vars['LANG']['word_show']; ?>
</label>
        <input type="radio" name="visibility" value="hide" id="st2" <?php if ($this->_tpl_vars['export_group_info']['visibility'] == 'hide'): ?>checked<?php endif; ?> />
          <label for="st2" class="red"><?php echo $this->_tpl_vars['LANG']['word_hide']; ?>
</label>
      </td>
    </tr>
    <tr>
      <td valign="top" class="medium_grey"><?php echo $this->_tpl_vars['L']['word_icon']; ?>
</td>
      <td>
        <input type="hidden" name="icon" id="icon" value="<?php echo $this->_tpl_vars['export_group_info']['icon']; ?>
" />
		<ul class="icon_list">
		  <li class="no_icon"></li>
	      <?php $_from = $this->_tpl_vars['icons']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['i'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['i']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['icon']):
        $this->_foreach['i']['iteration']++;
?>
	        <?php $this->assign('index', $this->_foreach['i']['iteration']); ?>
	        <li <?php if ($this->_tpl_vars['export_group_info']['icon'] == $this->_tpl_vars['icon']): ?>class="selected"<?php endif; ?>><img src="<?php echo $this->_tpl_vars['g_root_url']; ?>
/modules/export_manager/images/icons/<?php echo $this->_tpl_vars['icon']; ?>
" /></li>
	        <?php endforeach; endif; unset($_from); ?>
		</ul>
      </td>
    </tr>
    <tr>
      <td valign="top" class="medium_grey"><?php echo $this->_tpl_vars['L']['word_action']; ?>
</td>
      <td>

        <div>
          <input type="radio" name="action" value="file" id="action1" <?php if ($this->_tpl_vars['export_group_info']['action'] == 'file'): ?>checked<?php endif; ?>
            onclick="page_ns.change_action_type(this.value)" />
            <label for="action1"><?php echo $this->_tpl_vars['L']['phrase_generate_file']; ?>
</label>
        </div>
        <div>
          <input type="radio" name="action" value="new_window" id="action2" <?php if ($this->_tpl_vars['export_group_info']['action'] == 'new_window'): ?>checked<?php endif; ?>
            onclick="page_ns.change_action_type(this.value)" />
            <label for="action2"><?php echo $this->_tpl_vars['L']['phrase_open_in_new_window']; ?>
</label>
        </div>
        <div>
          <input type="radio" name="action" value="popup" id="action3" <?php if ($this->_tpl_vars['export_group_info']['action'] == 'popup'): ?>checked<?php endif; ?>
            onclick="page_ns.change_action_type(this.value)" />
            <label for="action3"><?php echo $this->_tpl_vars['L']['phrase_display_popup']; ?>
</label>
			<span class="light_grey">&#8212;</span>
			<?php echo $this->_tpl_vars['L']['word_height_c']; ?>

			<input type="text" name="popup_height" value="<?php echo $this->_tpl_vars['export_group_info']['popup_height']; ?>
" style="width:40px" />px
			<span class="light_grey">&#8212;</span>
			<?php echo $this->_tpl_vars['L']['word_width_c']; ?>

			<input type="text" name="popup_width" value="<?php echo $this->_tpl_vars['export_group_info']['popup_width']; ?>
" style="width:40px" />px
        </div>
      </td>
    </tr>
    <tr>
      <td class="medium_grey"><?php echo $this->_tpl_vars['L']['phrase_action_button_text']; ?>
</td>
      <td>
        <input type="text" name="action_button_text" maxlength="100" style="width:100%" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['export_group_info']['action_button_text'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" />
      </td>
    </tr>
    <tr>
      <td valign="top" class="medium_grey"><?php echo $this->_tpl_vars['L']['word_headers']; ?>
</td>
      <td>

        <div style="border: 1px solid #666666; padding: 3px">
          <textarea style="width:100%; height: 80px;" name="headers" id="headers"
           <?php if ($this->_tpl_vars['export_group_info']['action'] == 'file'): ?>disabled<?php endif; ?>><?php echo $this->_tpl_vars['export_group_info']['headers']; ?>
</textarea>
        </div>

        <script type="text/javascript">
        var html_editor = new CodeMirror.fromTextArea("headers", <?php echo '{'; ?>

        parserfile: ["parsexml.js"],
        path: "<?php echo $this->_tpl_vars['g_root_url']; ?>
/global/codemirror/js/",
        stylesheet: "<?php echo $this->_tpl_vars['g_root_url']; ?>
/global/codemirror/css/xmlcolors.css"
        <?php echo '});'; ?>

        </script>

      </td>
    </tr>
    <tr>
      <td valign="top" class="medium_grey"><?php echo $this->_tpl_vars['L']['phrase_smarty_template']; ?>
</td>
      <td>
        <div style="border: 1px solid #666666; padding: 3px">
          <textarea style="width:100%; height: 300px;" name="smarty_template" id="smarty_template"><?php echo $this->_tpl_vars['export_group_info']['smarty_template']; ?>
</textarea>
        </div>

        <script type="text/javascript">
        var html_editor = new CodeMirror.fromTextArea("smarty_template", <?php echo '{'; ?>

        parserfile: ["parsexml.js"],
        path: "<?php echo $this->_tpl_vars['g_root_url']; ?>
/global/codemirror/js/",
        stylesheet: "<?php echo $this->_tpl_vars['g_root_url']; ?>
/global/codemirror/css/xmlcolors.css"
        <?php echo '});'; ?>

        </script>
      </td>
    </tr>
    </table>

    <p>
      <input type="submit" name="update_export_group" value="<?php echo $this->_tpl_vars['LANG']['word_update']; ?>
" />
    </p>

  </form>