<?php /* Smarty version 2.6.18, created on 2016-04-14 06:04:12
         compiled from ../../modules/export_manager/templates/export_groups/tab_permissions.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'clients_dropdown', '../../modules/export_manager/templates/export_groups/tab_permissions.tpl', 40, false),array('modifier', 'in_array', '../../modules/export_manager/templates/export_groups/tab_permissions.tpl', 90, false),)), $this); ?>
  <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'messages.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

  <form action="<?php echo $this->_tpl_vars['same_page']; ?>
" method="post">
    <input type="hidden" name="export_group_id" value="<?php echo $this->_tpl_vars['export_group_info']['export_group_id']; ?>
" />

    <table cellspacing="1" cellpadding="2" border="0" width="100%">
    <tr>
      <td width="130" class="medium_grey" valign="top"><?php echo $this->_tpl_vars['LANG']['phrase_access_type']; ?>
</td>
      <td>
        <table cellspacing="1" cellpadding="0" class="margin_bottom">
        <tr>
          <td>
            <input type="radio" name="access_type" id="at1" value="admin" <?php if ($this->_tpl_vars['export_group_info']['access_type'] == 'admin'): ?>checked<?php endif; ?> />
              <label for="at1"><?php echo $this->_tpl_vars['LANG']['phrase_admin_only']; ?>
</label>
          </td>
        </tr>
        <tr>
          <td>
            <input type="radio" name="access_type" id="at2" value="public" <?php if ($this->_tpl_vars['export_group_info']['access_type'] == 'public'): ?>checked<?php endif; ?> />
              <label for="at2"><?php echo $this->_tpl_vars['LANG']['word_public']; ?>
 <span class="light_grey">(all clients have access)</span></label>
          </td>
        </tr>
        <tr>
          <td>
            <input type="radio" name="access_type" id="at3" value="private" <?php if ($this->_tpl_vars['export_group_info']['access_type'] == 'private'): ?>checked<?php endif; ?> />
              <label for="at3"><?php echo $this->_tpl_vars['LANG']['word_private']; ?>
 <span class="light_grey">(only specific clients have access)</span></label>
          </td>
        </tr>
        </table>

        <div id="custom_clients" <?php if ($this->_tpl_vars['export_group_info']['access_type'] != 'private'): ?>style="display:none"<?php endif; ?>>
          <table cellpadding="0" cellspacing="0" class="subpanel">
          <tr>
            <td class="medium_grey"><?php echo $this->_tpl_vars['LANG']['phrase_available_clients']; ?>
</td>
            <td></td>
            <td class="medium_grey"><?php echo $this->_tpl_vars['LANG']['phrase_selected_clients']; ?>
</td>
          </tr>
          <tr>
            <td>
              <?php echo smarty_function_clients_dropdown(array('name_id' => "available_client_ids[]",'multiple' => 'true','multiple_action' => 'hide','clients' => $this->_tpl_vars['export_group_info']['client_ids'],'size' => '4','style' => "width: 220px"), $this);?>

            </td>
            <td align="center" valign="middle" width="100">
              <input type="button" value="<?php echo $this->_tpl_vars['LANG']['word_add_uc_rightarrow']; ?>
"
                onclick="ft.move_options(this.form['available_client_ids[]'], this.form['selected_client_ids[]']);" /><br />
              <input type="button" value="<?php echo $this->_tpl_vars['LANG']['word_remove_uc_leftarrow']; ?>
"
                onclick="ft.move_options(this.form['selected_client_ids[]'], this.form['available_client_ids[]']);" />
            </td>
            <td>
              <?php echo smarty_function_clients_dropdown(array('name_id' => "selected_client_ids[]",'multiple' => 'true','multiple_action' => 'show','clients' => $this->_tpl_vars['export_group_info']['client_ids'],'size' => '4','style' => "width: 220px"), $this);?>

            </td>
          </tr>
          </table>
        </div>

      </td>
    </tr>
    <tr>
      <td class="medium_grey" valign="top">Where Shown</td>
      <td>

	    <div>
	      <input type="radio" name="form_view_mapping" id="fvm1" value="all" <?php if ($this->_tpl_vars['export_group_info']['form_view_mapping'] == 'all'): ?>checked<?php endif; ?> />
	      <label for="fvm1">All forms and Views</label>
	    </div>
	    <div>
	      <input type="radio" name="form_view_mapping" id="fvm2" value="except" <?php if ($this->_tpl_vars['export_group_info']['form_view_mapping'] == 'except'): ?>checked<?php endif; ?> />
	      <label for="fvm2">All forms and Views, <b>except</b>:</label>
	    </div>
	    <div class="margin_bottom">
	      <input type="radio" name="form_view_mapping" id="fvm3" value="only" <?php if ($this->_tpl_vars['export_group_info']['form_view_mapping'] == 'only'): ?>checked<?php endif; ?> />
	      <label for="fvm3"><b>Only</b> forms and Views:</label>
	    </div>


        <div id="custom_forms" <?php if ($this->_tpl_vars['export_group_info']['form_view_mapping'] == 'all'): ?>style="display:none"<?php endif; ?> class="margin_top">
          <table cellpadding="0" cellspacing="0" class="subpanel">
          <tr>
            <td class="medium_grey" width="50%"><?php echo $this->_tpl_vars['LANG']['word_forms']; ?>
</td>
            <td class="medium_grey" width="50%"><?php echo $this->_tpl_vars['LANG']['word_views']; ?>
</td>
          </tr>
          <tr>
            <td height="200">
              <div class="overflow_panel">
                <ul>
                <?php $_from = $this->_tpl_vars['forms']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['form_info']):
?>
                  <li>
                    <input type="checkbox" name="form_ids[]" class="form_ids" id="f<?php echo $this->_tpl_vars['form_info']['form_id']; ?>
" value="<?php echo $this->_tpl_vars['form_info']['form_id']; ?>
"
                      <?php if (((is_array($_tmp=$this->_tpl_vars['form_info']['form_id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['selected_form_ids']) : in_array($_tmp, $this->_tpl_vars['selected_form_ids']))): ?>checked<?php endif; ?> />
                      <label for="f<?php echo $this->_tpl_vars['form_info']['form_id']; ?>
"><?php echo $this->_tpl_vars['form_info']['form_name']; ?>
</label>
                  </li>
                <?php endforeach; endif; unset($_from); ?>
                </ul>
              </div>
            </td>
            <td height="200">
              <div class="overflow_panel">

                <?php $_from = $this->_tpl_vars['forms']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['form_info']):
?>
                  <?php $this->assign('form_id', $this->_tpl_vars['form_info']['form_id']); ?>
                  <div class="view_group" id="f<?php echo $this->_tpl_vars['form_id']; ?>
_views" <?php if (! ((is_array($_tmp=$this->_tpl_vars['form_id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['selected_form_ids']) : in_array($_tmp, $this->_tpl_vars['selected_form_ids']))): ?>style="display:none"<?php endif; ?>>
                    <h2><?php echo $this->_tpl_vars['form_info']['form_name']; ?>
</h2>
                    <ul>
                      <li>
                        <?php $this->assign('all_views_checked', ((is_array($_tmp="form".($this->_tpl_vars['form_id'])."_all_views")) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['selected_view_ids']) : in_array($_tmp, $this->_tpl_vars['selected_view_ids']))); ?>
                        <input type="checkbox" name="view_ids[]" id="form<?php echo $this->_tpl_vars['form_id']; ?>
_all_views" value="form<?php echo $this->_tpl_vars['form_id']; ?>
_all_views" class="view_ids all_views"
                          <?php if ($this->_tpl_vars['all_views_checked']): ?>checked<?php endif; ?> />
                          <label for="form<?php echo $this->_tpl_vars['form_id']; ?>
_all_views" class="all_views_label">&#8212; All Views &#8212;</label>
                      </li>
                      <?php $_from = $this->_tpl_vars['form_info']['views']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['view_info']):
?>
                      <?php $this->assign('view_id', $this->_tpl_vars['view_info']['view_id']); ?>
                      <li>
                        <input type="checkbox" name="view_ids[]" id="v<?php echo $this->_tpl_vars['view_id']; ?>
" value="<?php echo $this->_tpl_vars['view_id']; ?>
" class="view_ids"
                          <?php if ($this->_tpl_vars['all_views_checked']): ?>
                            disabled
                          <?php else: ?>
                            <?php if (((is_array($_tmp=$this->_tpl_vars['view_id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['selected_view_ids']) : in_array($_tmp, $this->_tpl_vars['selected_view_ids']))): ?>checked<?php endif; ?>
                          <?php endif; ?> />
                          <label for="v<?php echo $this->_tpl_vars['view_id']; ?>
"><?php echo $this->_tpl_vars['view_info']['view_name']; ?>
</label>
                      </li>
                      <?php endforeach; endif; unset($_from); ?>
                    </ul>
                  </div>
                <?php endforeach; endif; unset($_from); ?>

              </div>
            </td>
          </tr>
          </table>
        </div>

      </td>
    </tr>
    </table>

    <p>
      <input type="submit" name="update_permissions" value="<?php echo $this->_tpl_vars['LANG']['word_update']; ?>
" />
    </p>

  </form>