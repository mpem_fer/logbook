<?php /* Smarty version 2.6.18, created on 2016-04-14 05:58:17
         compiled from C:%5Cxampp%5Chtdocs%5Clogbook/themes/default/../../modules/data_visualization/templates/field_charts/tab_main.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'upper', 'C:\\xampp\\htdocs\\logbook/themes/default/../../modules/data_visualization/templates/field_charts/tab_main.tpl', 5, false),array('modifier', 'escape', 'C:\\xampp\\htdocs\\logbook/themes/default/../../modules/data_visualization/templates/field_charts/tab_main.tpl', 13, false),array('function', 'forms_dropdown', 'C:\\xampp\\htdocs\\logbook/themes/default/../../modules/data_visualization/templates/field_charts/tab_main.tpl', 19, false),array('function', 'views_dropdown', 'C:\\xampp\\htdocs\\logbook/themes/default/../../modules/data_visualization/templates/field_charts/tab_main.tpl', 25, false),array('function', 'cache_frequency_dropdown', 'C:\\xampp\\htdocs\\logbook/themes/default/../../modules/data_visualization/templates/field_charts/tab_main.tpl', 41, false),)), $this); ?>
  <form action="<?php echo $this->_tpl_vars['same_page']; ?>
" method="post" onsubmit="return rsv.validate(this, rules)">
    <input type="hidden" name="vis_id" value="<?php echo $this->_tpl_vars['vis_info']['vis_id']; ?>
" />
    <input type="hidden" name="tab" value="main" />

    <div class="subtitle underline margin_top_large"><?php echo ((is_array($_tmp=$this->_tpl_vars['LANG']['phrase_main_settings'])) ? $this->_run_mod_handler('upper', true, $_tmp) : smarty_modifier_upper($_tmp)); ?>
</div>

    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "messages.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

    <table cellspacing="1" cellpadding="0" class="list_table">
    <tr>
      <td class="pad_left_small" width="180"><?php echo $this->_tpl_vars['L']['phrase_visualization_name']; ?>
</td>
      <td>
        <input type="text" name="vis_name" id="vis_name" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['vis_info']['vis_name'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" style="width: 99%" />
      </td>
    </tr>
    <tr>
      <td class="pad_left_small"><?php echo $this->_tpl_vars['LANG']['word_form']; ?>
</td>
      <td>
        <?php echo smarty_function_forms_dropdown(array('name_id' => 'form_id','include_blank_option' => true,'default' => $this->_tpl_vars['vis_info']['form_id']), $this);?>

      </td>
    </tr>
    <tr>
      <td class="pad_left_small"><?php echo $this->_tpl_vars['LANG']['word_view']; ?>
</td>
      <td>
        <?php echo smarty_function_views_dropdown(array('form_id' => $this->_tpl_vars['vis_info']['form_id'],'name_id' => 'view_id','selected' => $this->_tpl_vars['vis_info']['view_id'],'omit_hidden_views' => true), $this);?>

      </td>
    </tr>
    <tr>
      <td class="pad_left_small"><?php echo $this->_tpl_vars['LANG']['word_field']; ?>
</td>
      <td>
        <select name="field_id" id="field_id">
        <?php $_from = $this->_tpl_vars['form_fields']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['field_info']):
?>
          <option value="<?php echo $this->_tpl_vars['field_info']['field_id']; ?>
" <?php if ($this->_tpl_vars['field_info']['field_id'] == $this->_tpl_vars['vis_info']['field_id']): ?>selected<?php endif; ?>><?php echo $this->_tpl_vars['field_info']['field_title']; ?>
</option>
        <?php endforeach; endif; unset($_from); ?>
        </select>
      </td>
    </tr>
    <tr>
      <td class="pad_left_small"><?php echo $this->_tpl_vars['L']['phrase_cache_update_frequency']; ?>
</td>
      <td>
        <?php echo smarty_function_cache_frequency_dropdown(array('name_id' => 'cache_update_frequency','default' => $this->_tpl_vars['vis_info']['cache_update_frequency']), $this);?>

        <div class="hint">
          <?php echo $this->_tpl_vars['L']['text_cache_frequency_explanation']; ?>

        </div>
      </td>
    </tr>
    </table>

    <p>
      <input type="button" id="delete_visualization" value="<?php echo $this->_tpl_vars['L']['phrase_delete_visualization']; ?>
" class="burgundy right" />
      <input type="submit" name="update" value="<?php echo $this->_tpl_vars['LANG']['word_update']; ?>
" />
    </p>

  </form>