<?php /* Smarty version 2.6.18, created on 2016-04-14 05:56:03
         compiled from C:%5Cxampp%5Chtdocs%5Clogbook/modules/data_visualization/templates/activity_charts/add.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'upper', 'C:\\xampp\\htdocs\\logbook/modules/data_visualization/templates/activity_charts/add.tpl', 44, false),array('function', 'forms_dropdown', 'C:\\xampp\\htdocs\\logbook/modules/data_visualization/templates/activity_charts/add.tpl', 56, false),array('function', 'activity_chart_date_range', 'C:\\xampp\\htdocs\\logbook/modules/data_visualization/templates/activity_charts/add.tpl', 71, false),array('function', 'colour_dropdown', 'C:\\xampp\\htdocs\\logbook/modules/data_visualization/templates/activity_charts/add.tpl', 97, false),array('function', 'line_width_dropdown', 'C:\\xampp\\htdocs\\logbook/modules/data_visualization/templates/activity_charts/add.tpl', 104, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'modules_header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

  <table cellpadding="0" cellspacing="0">
  <tr>
    <td width="45"><a href="../index.php"><img src="../images/icon_visualization.png" border="0" width="34" height="34" /></a></td>
    <td class="title">
      <a href="../../../admin/modules"><?php echo $this->_tpl_vars['LANG']['word_modules']; ?>
</a>
      <span class="joiner">&raquo;</span>
      <a href="../"><?php echo $this->_tpl_vars['L']['module_name']; ?>
</a>
      <span class="joiner">&raquo;</span>
      <?php echo $this->_tpl_vars['L']['phrase_create_new_activity_chart']; ?>

    </td>
  </tr>
  </table>

  <?php if ($this->_tpl_vars['g_message']): ?>

    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "messages.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

    <div><b><?php echo $this->_tpl_vars['L']['word_actions']; ?>
</b></div>
    <ul>
      <li><a href="../"><?php echo $this->_tpl_vars['L']['phrase_list_visualizations']; ?>
</a></li>
      <li><a href="add.php"><?php echo $this->_tpl_vars['L']['phrase_create_a_new_activity_chart']; ?>
</a></li>
      <?php if ($this->_tpl_vars['g_success']): ?>
        <li><a href="edit.php?page=main&vis_id=<?php echo $this->_tpl_vars['vis_id']; ?>
"><?php echo $this->_tpl_vars['L']['phrase_edit_the_activity_chart']; ?>
</a></li>
      <?php endif; ?>
      <?php if ($this->_tpl_vars['form_id'] && $this->_tpl_vars['view_id']): ?>
        <li><a href="../../../admin/forms/submissions.php?form_id=<?php echo $this->_tpl_vars['form_id']; ?>
&view_id=<?php echo $this->_tpl_vars['view_id']; ?>
"><?php echo $this->_tpl_vars['L']['phrase_view_form_submissions']; ?>
</a></li>
      <?php endif; ?>
    </ul>

  <?php else: ?>

    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "messages.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "../../modules/data_visualization/no_internet_connection.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

    <div class="margin_bottom_large">
      <?php echo $this->_tpl_vars['L']['text_add_visualization']; ?>

    </div>

	  <form action="<?php echo $this->_tpl_vars['same_page']; ?>
" method="post" onsubmit="return rsv.validate(this, rules)">

	    <div class="subtitle underline margin_bottom_large"><?php echo ((is_array($_tmp=$this->_tpl_vars['LANG']['phrase_main_settings'])) ? $this->_run_mod_handler('upper', true, $_tmp) : smarty_modifier_upper($_tmp)); ?>
</div>

	    <table cellspacing="1" cellpadding="0" class="list_table margin_bottom_large">
	    <tr>
	      <td class="pad_left_small" width="180"><?php echo $this->_tpl_vars['L']['phrase_visualization_name']; ?>
</td>
	      <td>
	        <input type="text" name="vis_name" id="vis_name" value="Form Activity" style="width: 99%" />
	      </td>
	    </tr>
	    <tr>
	      <td class="pad_left_small"><?php echo $this->_tpl_vars['LANG']['word_form']; ?>
</td>
	      <td>
	        <?php echo smarty_function_forms_dropdown(array('name_id' => 'form_id','omit_forms' => $this->_tpl_vars['omit_forms'],'include_blank_option' => true), $this);?>

	      </td>
	    </tr>
	    </table>

	    <table cellspacing="0" cellpadding="0" width="100%">
	    <tr>
	      <td valign="top">

	        <div class="subtitle underline margin_bottom_large"><?php echo ((is_array($_tmp=$this->_tpl_vars['L']['word_appearance'])) ? $this->_run_mod_handler('upper', true, $_tmp) : smarty_modifier_upper($_tmp)); ?>
</div>

	        <table cellspacing="0" cellpadding="1" class="list_table margin_bottom_large">
	        <tr>
	          <td class="pad_left_small">Date Range to display</td>
	          <td>
              <?php echo smarty_function_activity_chart_date_range(array('name_id' => 'date_range','default' => $this->_tpl_vars['module_settings']['activity_chart_date_range']), $this);?>

	          </td>
	        </tr>
	        <tr>
	          <td class="pad_left_small">Group submission count by</td>
	          <td >
	            <input type="radio" name="submission_count_group" id="scd1" value="month" <?php if ($this->_tpl_vars['module_settings']['activity_chart_submission_count_group'] == 'month'): ?>checked<?php endif; ?> />
	              <label for="scd1"><?php echo $this->_tpl_vars['L']['word_month']; ?>
</label>
	            <input type="radio" name="submission_count_group" id="scd3" value="day" <?php if ($this->_tpl_vars['module_settings']['activity_chart_submission_count_group'] == 'day'): ?>checked<?php endif; ?> />
	              <label for="scd3"><?php echo $this->_tpl_vars['L']['word_day']; ?>
</label>
	          </td>
	        </tr>
	        <tr>
	          <td class="pad_left_small"><?php echo $this->_tpl_vars['L']['phrase_chart_type']; ?>
</td>
	          <td>
	            <input type="radio" name="chart_type" id="lc1" value="line_chart" <?php if ($this->_tpl_vars['module_settings']['activity_chart_default_chart_type'] == 'line_chart'): ?>checked<?php endif; ?> />
	              <label for="lc1"><?php echo $this->_tpl_vars['L']['phrase_line_chart']; ?>
</label>
	            <input type="radio" name="chart_type" id="lc2" value="area_chart" <?php if ($this->_tpl_vars['module_settings']['activity_chart_default_chart_type'] == 'area_chart'): ?>checked<?php endif; ?> />
	              <label for="lc2"><?php echo $this->_tpl_vars['L']['phrase_area_chart']; ?>
</label>
              <input type="radio" name="chart_type" id="lc3" value="column_chart" <?php if ($this->_tpl_vars['module_settings']['activity_chart_default_chart_type'] == 'column_chart'): ?>checked<?php endif; ?> />
                <label for="lc3"><?php echo $this->_tpl_vars['L']['phrase_column_chart']; ?>
</label>
	          </td>
	        </tr>
	        <tr>
	          <td class="pad_left_small"><?php echo $this->_tpl_vars['L']['word_colour']; ?>
</td>
	          <td>
	            <?php echo smarty_function_colour_dropdown(array('name_id' => 'colour','default' => $this->_tpl_vars['module_settings']['activity_chart_colour']), $this);?>

	          </td>
	        </tr>
	        <tr>
	          <td class="pad_left_small"><?php echo $this->_tpl_vars['L']['phrase_line_width']; ?>
</td>
	          <td>
	            	            <?php echo smarty_function_line_width_dropdown(array('name_id' => 'line_width','default' => $this->_tpl_vars['module_settings']['activity_chart_line_width']), $this);?>
 px
	            <div class="hint">This is for Line and Area charts only.</div>
	          </td>
	        </tr>
	        </table>
	      </td>
	      <td width="250" valign="top">
          <div class="subtitle underline margin_bottom_large"><?php echo $this->_tpl_vars['L']['word_thumbnail']; ?>
</div>
	        <div id="thumb_chart"></div>
	      </td>
	    </tr>
	    </table>

      <div class="subtitle underline margin_bottom_large"><?php echo $this->_tpl_vars['L']['phrase_full_size']; ?>
</div>
      <div id="full_size_chart"></div>

	    <p>
	      <input type="submit" name="add" value="<?php echo $this->_tpl_vars['L']['phrase_create_visualization']; ?>
" />
	    </p>

	  </form>

  <?php endif; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'modules_footer.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>