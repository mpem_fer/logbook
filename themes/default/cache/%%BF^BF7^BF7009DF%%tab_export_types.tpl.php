<?php /* Smarty version 2.6.18, created on 2016-04-14 06:04:14
         compiled from ../../modules/export_manager/templates/export_groups/tab_export_types.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', '../../modules/export_manager/templates/export_groups/tab_export_types.tpl', 6, false),array('modifier', 'upper', '../../modules/export_manager/templates/export_groups/tab_export_types.tpl', 17, false),array('function', 'eval', '../../modules/export_manager/templates/export_groups/tab_export_types.tpl', 36, false),)), $this); ?>
  <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'messages.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

  <form action="<?php echo $this->_tpl_vars['same_page']; ?>
" method="post">
    <input type="hidden" name="export_group_id" value="<?php echo $this->_tpl_vars['export_group_info']['export_group_id']; ?>
" />

    <?php if (count($this->_tpl_vars['export_types']) == 0): ?>
      <div class="notify yellow_bg" class="margin_bottom_large">
        <div style="padding:8px">
          <?php echo $this->_tpl_vars['L']['notify_no_export_types']; ?>

        </div>
      </div>
    <?php else: ?>
      <div class="sortable export_type_list" id="<?php echo $this->_tpl_vars['sortable_id']; ?>
">
        <input type="hidden" class="sortable__custom_delete_handler" value="em_ns.delete_export_type" />
        <ul class="header_row">
          <li class="col1"><?php echo $this->_tpl_vars['LANG']['word_order']; ?>
</li>
          <li class="col2"><?php echo ((is_array($_tmp=$this->_tpl_vars['LANG']['word_id'])) ? $this->_run_mod_handler('upper', true, $_tmp) : smarty_modifier_upper($_tmp)); ?>
</li>
          <li class="col3"><?php echo $this->_tpl_vars['L']['phrase_export_type']; ?>
</li>
          <li class="col4"><?php echo $this->_tpl_vars['L']['word_visibility']; ?>
</li>
          <li class="col5 edit"></li>
          <li class="col6 colN del"></li>
        </ul>
        <div class="clear"></div>
        <ul class="rows" id="rows">
        <?php $_from = $this->_tpl_vars['export_types']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['row'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['row']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['export_type']):
        $this->_foreach['row']['iteration']++;
?>
          <?php $this->assign('index', ($this->_foreach['row']['iteration']-1)); ?>
          <?php $this->assign('count', $this->_foreach['row']['iteration']); ?>
          <?php $this->assign('export_type_id', $this->_tpl_vars['export_type']['export_type_id']); ?>
          <li class="sortable_row">
            <div class="row_content">
              <div class="row_group<?php if (($this->_foreach['row']['iteration'] == $this->_foreach['row']['total'])): ?> rowN<?php endif; ?>">
                <input type="hidden" class="sr_order" value="<?php echo $this->_tpl_vars['export_type_id']; ?>
" />
                <ul>
                  <li class="col1 sort_col"><?php echo $this->_tpl_vars['count']; ?>
</li>
                  <li class="col2"><?php echo $this->_tpl_vars['export_type_id']; ?>
</li>
                  <li class="col3"><?php echo smarty_function_eval(array('var' => $this->_tpl_vars['export_type']['export_type_name']), $this);?>
</li>
                  <li class="col4">
                    <?php if ($this->_tpl_vars['export_type']['export_type_visibility'] == 'show'): ?>
                      <span class="green"><?php echo $this->_tpl_vars['LANG']['word_show']; ?>
</span>
                    <?php else: ?>
                      <span class="red"><?php echo $this->_tpl_vars['LANG']['word_hide']; ?>
</span>
                    <?php endif; ?>
                  </li>
                  <li class="col5 edit"><a href="edit.php?page=edit_export_type&export_type_id=<?php echo $this->_tpl_vars['export_type_id']; ?>
"></a></li>
                  <li class="col6 colN del"></li>
                </ul>
                <div class="clear"></div>
              </div>
            </div>
            <div class="clear"></div>
          </li>
        <?php endforeach; endif; unset($_from); ?>
        </ul>
      </div>

    <?php endif; ?>

    <p>
      <?php if (count($this->_tpl_vars['export_types']) > 0): ?>
        <input type="submit" name="reorder_export_types" value="<?php echo $this->_tpl_vars['LANG']['phrase_update_order']; ?>
" />
      <?php endif; ?>
      <input type="submit" name="create_export_type" value="<?php echo $this->_tpl_vars['L']['phrase_add_export_type']; ?>
" />
    </p>

  </form>