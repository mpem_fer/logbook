<?php /* Smarty version 2.6.18, created on 2016-04-14 06:04:47
         compiled from C:%5Cxampp%5Chtdocs%5Clogbook/modules/export_manager/templates/settings.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'escape', 'C:\\xampp\\htdocs\\logbook/modules/export_manager/templates/settings.tpl', 29, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'modules_header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

  <table cellpadding="0" cellspacing="0">
  <tr>
    <td width="45"><a href="index.php"><img src="images/icon_export.gif" border="0" width="34" height="34" /></a></td>
    <td class="title">
      <a href="../../admin/modules"><?php echo $this->_tpl_vars['LANG']['word_modules']; ?>
</a>
      <span class="joiner">&raquo;</span>
      <a href="./"><?php echo $this->_tpl_vars['L']['module_name']; ?>
</a>
      <span class="joiner">&raquo;</span>
      <?php echo $this->_tpl_vars['L']['word_settings']; ?>

    </td>
  </tr>
  </table>

  <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'messages.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

  <form action="<?php echo $this->_tpl_vars['same_page']; ?>
" method="post">
    <table cellpadding="0" cellspacing="1" class="list_table" width="100%">
    <tr>
      <td width="170" class="nowrap pad_left pad_right_large"><?php echo $this->_tpl_vars['LANG']['word_version']; ?>
</td>
      <td class="medium_grey pad_left"><?php echo $this->_tpl_vars['module_version']; ?>
</td>
    </tr>
    <tr>
      <td width="170" class="nowrap pad_left pad_right_large"><?php echo $this->_tpl_vars['L']['phrase_generate_files_folder_path']; ?>
</td>
      <td>
        <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
          <td><input type="text" name="file_upload_dir" id="file_upload_dir" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['module_settings']['file_upload_dir'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" style="width: 98%" /></td>
          <td width="180">
            <input type="button" value="<?php echo $this->_tpl_vars['LANG']['phrase_test_folder_permissions']; ?>
"
              onclick="ft.test_folder_permissions($('#file_upload_dir').val(), 'permissions_result')" style="width: 180px;" />
          </td>
        </tr>
        </table>
        <div id="permissions_result"></div>
      </td>
    </tr>
    <tr>
      <td class="pad_left"><?php echo $this->_tpl_vars['L']['phrase_generate_files_folder_url']; ?>
</td>
      <td>
        <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
          <td><input type="text" name="file_upload_url" id="file_upload_url" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['module_settings']['file_upload_url'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" style="width: 98%" /></td>
          <?php if ($this->_tpl_vars['allow_url_fopen']): ?>
            <td width="180"><input type="button" value="<?php echo $this->_tpl_vars['LANG']['phrase_confirm_folder_url_match']; ?>
"
              onclick="ft.test_folder_url_match($('#file_upload_dir').val(), $('#file_upload_url').val(), 'folder_match_message_id')"
              style="width: 180px;" /></td>
          <?php endif; ?>
        </tr>
        </table>
        <div id="folder_match_message_id"></div>
      </td>
    </tr>
<!--
    <tr>
      <td class="pad_left"><?php echo $this->_tpl_vars['L']['phrase_cache_multi_select_fields']; ?>
</td>
      <td>
        <input type="radio" name="cache_multi_select_fields" id="cmsf1" value="yes"
          <?php if ($this->_tpl_vars['module_settings']['cache_multi_select_fields'] == 'yes'): ?>checked<?php endif; ?> />
          <label for="cmsf1"><?php echo $this->_tpl_vars['LANG']['word_yes']; ?>
</label>
        <input type="radio" name="cache_multi_select_fields" id="cmsf2" value="no"
          <?php if ($this->_tpl_vars['module_settings']['cache_multi_select_fields'] == 'no'): ?>checked<?php endif; ?> />
          <label for="cmsf2"><?php echo $this->_tpl_vars['LANG']['word_no']; ?>
</label>
      </td>
    </tr>
-->
    </table>

    <p>
      <input type="submit" name="update" value="<?php echo $this->_tpl_vars['LANG']['word_update']; ?>
" />
    </p>

  </form>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'modules_footer.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>