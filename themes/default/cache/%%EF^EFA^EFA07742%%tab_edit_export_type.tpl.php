<?php /* Smarty version 2.6.18, created on 2016-04-14 06:04:16
         compiled from ../../modules/export_manager/templates/export_groups/tab_edit_export_type.tpl */ ?>
  <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'messages.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

  <form action="<?php echo $this->_tpl_vars['same_page']; ?>
" method="post" onsubmit="return rsv.validate(this, page_ns.rules)">
    <input type="hidden" name="export_type_id" value="<?php echo $this->_tpl_vars['export_type']['export_type_id']; ?>
" />
    <input type="hidden" name="export_group_id" value="<?php echo $this->_tpl_vars['export_type']['export_group_id']; ?>
" />
    <input type="hidden" name="page" value="edit_export_type" />

    <table cellspacing="1" cellpadding="2" border="0">
    <tr>
      <td width="120" class="medium_grey"><?php echo $this->_tpl_vars['L']['phrase_export_type_name']; ?>
</td>
      <td>
        <input type="text" name="export_type_name" value="<?php echo $this->_tpl_vars['export_type']['export_type_name']; ?>
" style="width:100%" maxlength="255" />
      </td>
    </tr>
    <tr>
      <td valign="top" class="medium_grey"><?php echo $this->_tpl_vars['L']['word_visibility']; ?>
</td>
      <td>
        <input type="radio" name="visibility" value="show" id="st1" <?php if ($this->_tpl_vars['export_type']['export_type_visibility'] == 'show'): ?>checked<?php endif; ?> />
          <label for="st1" class="green"><?php echo $this->_tpl_vars['LANG']['word_show']; ?>
</label>
        <input type="radio" name="visibility" value="hide" id="st2" <?php if ($this->_tpl_vars['export_type']['export_type_visibility'] == 'hide'): ?>checked<?php endif; ?> />
          <label for="st2" class="red"><?php echo $this->_tpl_vars['LANG']['word_hide']; ?>
</label>
        <div class="light_grey"><?php echo $this->_tpl_vars['L']['notify_export_type_visibility']; ?>
</div>
      </td>
    </tr>
    <tr>
    <tr>
      <td valign="top" class="medium_grey"><?php echo $this->_tpl_vars['L']['word_filename']; ?>
</td>
      <td>
        <input type="text" name="filename" value="<?php echo $this->_tpl_vars['export_type']['filename']; ?>
" style="width:100%" maxlength="50" />
        <div class="light_grey">
          <?php echo $this->_tpl_vars['L']['notify_filename_explanation']; ?>

        </div>
      </td>
    </tr>
    </table>

    <p class="bold"><?php echo $this->_tpl_vars['L']['phrase_smarty_template']; ?>
</p>

    <div style="border: 1px solid #666666; padding: 3px">
      <textarea name="smarty_template" id="smarty_template" style="width:100%; height:400px"><?php echo $this->_tpl_vars['export_type']['export_type_smarty_template']; ?>
</textarea>
    </div>

    <script>
    var html_editor = new CodeMirror.fromTextArea("smarty_template", <?php echo '{'; ?>

    parserfile: ["parsexml.js"],
    path: "<?php echo $this->_tpl_vars['g_root_url']; ?>
/global/codemirror/js/",
    stylesheet: "<?php echo $this->_tpl_vars['g_root_url']; ?>
/global/codemirror/css/xmlcolors.css"
    <?php echo '});'; ?>

    </script>

    <p>
      <input type="submit" name="update_export_type" value="<?php echo $this->_tpl_vars['LANG']['word_update']; ?>
" />
    </p>

  </form>