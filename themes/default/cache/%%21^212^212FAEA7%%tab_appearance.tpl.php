<?php /* Smarty version 2.6.18, created on 2016-04-14 05:59:10
         compiled from C:%5Cxampp%5Chtdocs%5Clogbook/themes/default/../../modules/data_visualization/templates/field_charts/tab_appearance.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'escape', 'C:\\xampp\\htdocs\\logbook/themes/default/../../modules/data_visualization/templates/field_charts/tab_appearance.tpl', 8, false),array('modifier', 'upper', 'C:\\xampp\\htdocs\\logbook/themes/default/../../modules/data_visualization/templates/field_charts/tab_appearance.tpl', 16, false),array('function', 'colour_dropdown', 'C:\\xampp\\htdocs\\logbook/themes/default/../../modules/data_visualization/templates/field_charts/tab_appearance.tpl', 48, false),)), $this); ?>
  <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "messages.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

  <form action="<?php echo $this->_tpl_vars['same_page']; ?>
" method="post">
    <input type="hidden" name="vis_id" id="vis_id" value="<?php echo $this->_tpl_vars['vis_info']['vis_id']; ?>
" />
    <input type="hidden" name="form_id" id="form_id" value="<?php echo $this->_tpl_vars['vis_info']['form_id']; ?>
" />
    <input type="hidden" name="view_id" id="view_id" value="<?php echo $this->_tpl_vars['vis_info']['view_id']; ?>
" />
    <input type="hidden" name="field_id" id="field_id" value="<?php echo $this->_tpl_vars['vis_info']['field_id']; ?>
" />
    <input type="hidden" name="vis_name" id="vis_name" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['vis_info']['vis_name'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" />
    <input type="hidden" name="tab" value="appearance" />

    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "../../modules/data_visualization/no_internet_connection.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

    <table cellspacing="0" cellpadding="0" width="100%" class="margin_bottom_large">
    <tr>
      <td valign="top">
        <div class="subtitle underline margin_bottom_large"><?php echo ((is_array($_tmp=$this->_tpl_vars['L']['word_appearance'])) ? $this->_run_mod_handler('upper', true, $_tmp) : smarty_modifier_upper($_tmp)); ?>
</div>

        <table cellspacing="0" cellpadding="1" class="list_table margin_bottom_large">
        <tr>
          <td class="pad_left_small"><?php echo $this->_tpl_vars['L']['phrase_chart_type']; ?>
</td>
          <td>
            <input type="radio" name="chart_type" id="ct1" value="pie_chart"
              <?php if ($this->_tpl_vars['vis_info']['chart_type'] == 'pie_chart'): ?>checked<?php endif; ?> />
              <label for="ct1"><?php echo $this->_tpl_vars['L']['phrase_pie_chart']; ?>
</label>
            <input type="radio" name="chart_type" id="ct2" value="bar_chart"
              <?php if ($this->_tpl_vars['vis_info']['chart_type'] == 'bar_chart'): ?>checked<?php endif; ?> />
              <label for="ct2"><?php echo $this->_tpl_vars['L']['phrase_bar_chart']; ?>
</label>
            <input type="radio" name="chart_type" id="ct3" value="column_chart"
              <?php if ($this->_tpl_vars['vis_info']['chart_type'] == 'column_chart'): ?>checked<?php endif; ?> />
              <label for="ct3"><?php echo $this->_tpl_vars['L']['phrase_column_chart']; ?>
</label>
          </td>
        </tr>
        <tr>
          <td class="pad_left_small"><?php echo $this->_tpl_vars['L']['phrase_ignore_fields_with_empty_vals']; ?>
</td>
          <td>
            <input type="radio" name="field_chart_ignore_empty_fields" id="ief1" value="yes"
              <?php if ($this->_tpl_vars['vis_info']['field_chart_ignore_empty_fields'] == 'yes'): ?>checked<?php endif; ?> />
              <label for="ief1"><?php echo $this->_tpl_vars['LANG']['word_yes']; ?>
</label>
            <input type="radio" name="field_chart_ignore_empty_fields" id="ief2" value="no"
              <?php if ($this->_tpl_vars['vis_info']['field_chart_ignore_empty_fields'] == 'no'): ?>checked<?php endif; ?> />
              <label for="ief2"><?php echo $this->_tpl_vars['LANG']['word_no']; ?>
</label>
          </td>
        </tr>
        <tr>
          <td class="pad_left_small"><?php echo $this->_tpl_vars['L']['word_colour']; ?>
</td>
          <td>
            <input type="hidden" name="colour_old" value="<?php echo $this->_tpl_vars['vis_info']['colour']; ?>
" />
            <?php echo smarty_function_colour_dropdown(array('name_id' => 'colour','default' => $this->_tpl_vars['vis_info']['colour']), $this);?>

            <div class="hint">
              <?php echo $this->_tpl_vars['L']['text_bar_and_col_charts_only']; ?>

            </div>
          </td>
        </tr>
        </table>

        <div class="subtitle underline margin_bottom_large"><?php echo ((is_array($_tmp=$this->_tpl_vars['L']['phrase_pie_chart_settings'])) ? $this->_run_mod_handler('upper', true, $_tmp) : smarty_modifier_upper($_tmp)); ?>
</div>

        <table cellspacing="0" cellpadding="1" class="list_table">
        <tr>
          <td width="190" class="pad_left_small"><?php echo $this->_tpl_vars['L']['phrase_pie_chart_format']; ?>
</td>
          <td>
            <input type="radio" name="pie_chart_format" id="pcf1" value="2D"
              <?php if ($this->_tpl_vars['vis_info']['pie_chart_format'] == '2D' || $this->_tpl_vars['vis_info']['pie_chart_format'] == ""): ?>checked<?php endif; ?>
              <?php if ($this->_tpl_vars['vis_info']['chart_type'] != 'pie_chart'): ?>disabled<?php endif; ?> />
              <label for="pcf1">2D</label>
            <input type="radio" name="pie_chart_format" id="pcf2" value="3D"
              <?php if ($this->_tpl_vars['vis_info']['pie_chart_format'] == '3D'): ?>checked<?php endif; ?>
              <?php if ($this->_tpl_vars['vis_info']['chart_type'] != 'pie_chart'): ?>disabled<?php endif; ?> />
              <label for="pcf2">3D</label>
          </td>
        </tr>
        <tr>
          <td class="pad_left_small"><?php echo $this->_tpl_vars['L']['phrase_include_legend_in_thumbnail']; ?>
</td>
          <td>
            <input type="radio" name="include_legend_quicklinks" id="ilq1" value="yes"
              <?php if ($this->_tpl_vars['vis_info']['include_legend_quicklinks'] == 'yes'): ?>checked<?php endif; ?>
              <?php if ($this->_tpl_vars['vis_info']['chart_type'] != 'pie_chart'): ?>disabled<?php endif; ?> />
              <label for="ilq1"><?php echo $this->_tpl_vars['LANG']['word_yes']; ?>
</label>
            <input type="radio" name="include_legend_quicklinks" id="ilq2" value="no"
              <?php if ($this->_tpl_vars['vis_info']['include_legend_quicklinks'] == 'no' || $this->_tpl_vars['vis_info']['include_legend_quicklinks'] == ""): ?>checked<?php endif; ?>
              <?php if ($this->_tpl_vars['vis_info']['chart_type'] != 'pie_chart'): ?>disabled<?php endif; ?> />
              <label for="ilq2"><?php echo $this->_tpl_vars['LANG']['word_no']; ?>
</label>
          </td>
        </tr>
        <tr>
          <td class="pad_left_small"><?php echo $this->_tpl_vars['L']['phrase_include_legend_in_full_size']; ?>
</td>
          <td>
            <input type="radio" name="include_legend_full_size" id="ilf1" value="yes"
              <?php if ($this->_tpl_vars['vis_info']['include_legend_full_size'] == 'yes' || $this->_tpl_vars['vis_info']['include_legend_full_size'] == ""): ?>checked<?php endif; ?>
              <?php if ($this->_tpl_vars['vis_info']['chart_type'] != 'pie_chart'): ?>disabled<?php endif; ?> />
              <label for="ilf1"><?php echo $this->_tpl_vars['LANG']['word_yes']; ?>
</label>
            <input type="radio" name="include_legend_full_size" id="ilf2" value="no"
              <?php if ($this->_tpl_vars['vis_info']['include_legend_full_size'] == 'no'): ?>checked<?php endif; ?>
              <?php if ($this->_tpl_vars['vis_info']['chart_type'] != 'pie_chart'): ?>disabled<?php endif; ?> />
              <label for="ilf2"><?php echo $this->_tpl_vars['LANG']['word_no']; ?>
</label>
          </td>
        </tr>
        </table>

      </td>
      <td width="250" valign="top">
        <div class="subtitle underline margin_bottom_large">&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['L']['word_thumbnail'])) ? $this->_run_mod_handler('upper', true, $_tmp) : smarty_modifier_upper($_tmp)); ?>
</div>
        <div id="thumb_chart"><div class="loading"></div></div>
      </td>
    </tr>
    </table>

    <div class="subtitle underline margin_bottom_large"><?php echo ((is_array($_tmp=$this->_tpl_vars['L']['phrase_full_size'])) ? $this->_run_mod_handler('upper', true, $_tmp) : smarty_modifier_upper($_tmp)); ?>
</div>
    <div id="full_size_chart"><div class="loading"></div></div>

    <p>
      <input type="button" id="delete_visualization" value="<?php echo $this->_tpl_vars['L']['phrase_delete_visualization']; ?>
" class="burgundy right" />
      <input type="submit" name="update" value="<?php echo $this->_tpl_vars['LANG']['word_update']; ?>
" />
    </p>

  </form>