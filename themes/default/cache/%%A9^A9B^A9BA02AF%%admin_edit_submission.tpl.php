<?php /* Smarty version 2.6.18, created on 2016-04-20 11:07:46
         compiled from C:%5Cxampp%5Chtdocs%5Clogbook/modules/submission_history/templates/admin_edit_submission.tpl */ ?>
<div class="module_section" id="sh__content">
  <div id="sh__loading"<?php if ($this->_tpl_vars['module_settings']['auto_load_on_edit_submission'] == 'no'): ?> style="display:none"<?php endif; ?>><img src="<?php echo $this->_tpl_vars['g_root_url']; ?>
/modules/submission_history/images/loading.gif" /></div>
  <div id="sh__load_history"<?php if ($this->_tpl_vars['module_settings']['auto_load_on_edit_submission'] == 'yes'): ?> style="display:none"<?php endif; ?>><input type="button" value="<?php echo $this->_tpl_vars['L']['phrase_load_history']; ?>
" onclick="sh.load_history()" /></div>
  <div id="sh__page_label"><?php echo $this->_tpl_vars['module_settings']['page_label']; ?>
</div>
  <div id="sh__results_div"></div>
</div>

<?php if ($this->_tpl_vars['module_settings']['auto_load_on_edit_submission'] == 'yes'): ?>
  <script>sh.load_history(1)</script>
<?php endif; ?>