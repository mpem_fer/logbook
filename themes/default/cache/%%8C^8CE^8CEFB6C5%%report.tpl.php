<?php /* Smarty version 2.6.18, created on 2016-02-01 04:01:20
         compiled from C:%5Cxampp%5Chtdocs%5Clogbook%5Cmodules%5Creport_builder%5Ctemplates%5Creport.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'C:\\xampp\\htdocs\\logbook\\modules\\report_builder\\templates\\report.tpl', 33, false),array('modifier', 'escape', 'C:\\xampp\\htdocs\\logbook\\modules\\report_builder\\templates\\report.tpl', 55, false),)), $this); ?>
  <?php if ($this->_tpl_vars['is_single_form'] && ! $this->_tpl_vars['is_valid_single_form']): ?>
    <div class="margin_bottom_large notify">
      <div style="padding: 6px">
        <?php echo $this->_tpl_vars['LANG']['report_builder']['notify_no_form_found']; ?>

      </div>
    </div>
  <?php else: ?>

    <?php if (! $this->_tpl_vars['is_single_form']): ?>
      <div class="margin_bottom_large">
        <div id="rb_expand_contract">
          <?php if ($this->_tpl_vars['expand_by_default'] == 'yes'): ?>
            <?php echo $this->_tpl_vars['LANG']['report_builder']['phrase_contract_all']; ?>

          <?php else: ?>
            <?php echo $this->_tpl_vars['LANG']['report_builder']['phrase_expand_all']; ?>

          <?php endif; ?>
        </div>
        <div id="rb_expand_label" class="hidden"><?php echo $this->_tpl_vars['LANG']['report_builder']['phrase_expand_all']; ?>
</div>
        <div id="rb_contract_label" class="hidden"><?php echo $this->_tpl_vars['LANG']['report_builder']['phrase_contract_all']; ?>
</div>
        <?php echo $this->_tpl_vars['LANG']['report_builder']['text_reports_intro']; ?>

      </div>
    <?php endif; ?>

    <?php $_from = $this->_tpl_vars['forms']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['row'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['row']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['form_info']):
        $this->_foreach['row']['iteration']++;
?>
      <?php $this->assign('index', ($this->_foreach['row']['iteration']-1)); ?>
      <?php $this->assign('count', $this->_foreach['row']['iteration']); ?>
      <?php $this->assign('form_id', $this->_tpl_vars['form_info']['form_id']); ?>
      <?php $this->assign('key', "form_".($this->_tpl_vars['form_id'])); ?>

      <div class="rb_form_section">
        <div class="rb_form_section_heading">
          <div class="rb_num_reports">
            <?php if (count($this->_tpl_vars['form_views'][$this->_tpl_vars['key']]) == 0): ?>
              0 <?php echo $this->_tpl_vars['LANG']['report_builder']['word_reports']; ?>

            <?php elseif (count($this->_tpl_vars['form_views'][$this->_tpl_vars['key']]) == 1): ?>
              1 <?php echo $this->_tpl_vars['LANG']['report_builder']['word_report']; ?>

            <?php else: ?>
              <?php echo count($this->_tpl_vars['form_views'][$this->_tpl_vars['key']]); ?>
 <?php echo $this->_tpl_vars['LANG']['report_builder']['word_reports']; ?>

            <?php endif; ?>
          </div>
          <div class="rb_form_name"><?php echo $this->_tpl_vars['form_info']['form_name']; ?>
</div>
          <div class="clear"></div>
        </div>
        <div class="<?php if ($this->_tpl_vars['is_single_form']): ?>rb_report_section_single_form<?php else: ?>rb_report_section<?php endif; ?>"<?php if ($this->_tpl_vars['expand_by_default'] == 'yes'): ?> style="display: block"<?php endif; ?>>
          <?php $_from = $this->_tpl_vars['form_views'][$this->_tpl_vars['key']]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['view_info']):
?>
            <div class="rb_view_row">
              <div class="rb_view_name"><?php echo $this->_tpl_vars['view_info']['view_name']; ?>
</div>
              <div class="rb_export_options">
                <div class="rb_num_submissions"><?php echo $this->_tpl_vars['LANG']['report_builder']['word_results_c']; ?>
 <?php echo $this->_tpl_vars['view_info']['num_submissions_in_view']; ?>
</div>
                <?php $_from = $this->_tpl_vars['export_options']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['export_option']):
?>
                  <?php $this->assign('group_id', $this->_tpl_vars['export_option']['group_id']); ?>
                  <div class="rb_export_option">
                    <a href="<?php echo $this->_tpl_vars['g_root_url']; ?>
/modules/report_builder/report.php?export_group_id=<?php echo $this->_tpl_vars['group_id']; ?>
&export_type_id=<?php echo $this->_tpl_vars['export_option']['export_type_id']; ?>
&export_group_<?php echo $this->_tpl_vars['group_id']; ?>
_results=all&form_id=<?php echo $this->_tpl_vars['form_id']; ?>
&view_id=<?php echo $this->_tpl_vars['view_info']['view_id']; ?>
"
                      target="_blank"><img src="<?php echo $this->_tpl_vars['g_root_url']; ?>
/modules/export_manager/images/icons/<?php echo $this->_tpl_vars['export_option']['icon']; ?>
"
                      title="<?php echo ((is_array($_tmp=$this->_tpl_vars['export_option']['group_name'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
 - <?php echo ((is_array($_tmp=$this->_tpl_vars['export_option']['export_type_name'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" /></a>
                  </div>
                <?php endforeach; endif; unset($_from); ?>
              </div>
              <div class="clear"></div>
            </div>
          <?php endforeach; endif; unset($_from); ?>
        </div>
      </div>
    <?php endforeach; endif; unset($_from); ?>
  <?php endif; ?>