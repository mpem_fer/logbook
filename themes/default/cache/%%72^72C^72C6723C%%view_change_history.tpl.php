<?php /* Smarty version 2.6.18, created on 2016-06-22 10:59:48
         compiled from C:%5Cxampp%5Chtdocs%5Clogbook/modules/submission_history/templates/view_change_history.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'display_custom_field', 'C:\\xampp\\htdocs\\logbook/modules/submission_history/templates/view_change_history.tpl', 51, false),)), $this); ?>
  <table cellpadding="0" cellspacing="0" class="margin_top_large margin_bottom_large">
    <tr>
      <td class="nowrap" width="60">
        <?php if ($this->_tpl_vars['previous_history_id']): ?>
          <a href="javascript:sh.view_history_changes(<?php echo $this->_tpl_vars['previous_history_id']; ?>
)"><?php echo $this->_tpl_vars['L']['word_older_leftarrow']; ?>
</a>
        <?php else: ?>
          <span class="light_grey"><?php echo $this->_tpl_vars['L']['word_older_leftarrow']; ?>
</a>
        <?php endif; ?>
      </td>
      <td class="nowrap" width="150" align="center"><a href="javascript:sh.back_to_history()"><?php echo $this->_tpl_vars['L']['phrase_back_to_history']; ?>
</a></td>
      <td class="nowrap">
        <?php if ($this->_tpl_vars['next_history_id']): ?>
          <a href="javascript:sh.view_history_changes(<?php echo $this->_tpl_vars['next_history_id']; ?>
)"><?php echo $this->_tpl_vars['L']['word_newer_rightarrow']; ?>
</a>
        <?php else: ?>
          <span class="light_grey"><?php echo $this->_tpl_vars['L']['word_newer_rightarrow']; ?>
</span>
        <?php endif; ?>
      </td>
    </tr>
  </table>

  <table cellspacing="0" cellpadding="0" class="change_type_info">
  <tr>
    <td width="120"><?php echo $this->_tpl_vars['L']['phrase_change_type']; ?>
</td>
    <td>
      <?php if ($this->_tpl_vars['item']['sh___change_type'] == 'new'): ?>
        <span class="change_type_new"><?php echo $this->_tpl_vars['L']['word_new']; ?>
</span>
      <?php elseif ($this->_tpl_vars['item']['sh___change_type'] == 'update'): ?>
        <span class="change_type_update"><?php echo $this->_tpl_vars['L']['word_update']; ?>
</span>
      <?php elseif ($this->_tpl_vars['item']['sh___change_type'] == 'restore'): ?>
        <span class="change_type_restore"><?php echo $this->_tpl_vars['L']['word_restored']; ?>
</span>
      <?php endif; ?>
    </td>
  </tr>
  </table>

  <ul>
    <li>
      <div>
        <div class="sh__field_name bold"><?php echo $this->_tpl_vars['L']['word_field']; ?>
</div>
        <div class="sh__previous_value bold"><?php echo $this->_tpl_vars['L']['phrase_previous_edit_value']; ?>
</div>
        <div class="sh__new_value bold"><?php echo $this->_tpl_vars['L']['phrase_new_edit_value']; ?>
</div>
      </div>
      <div style="clear: both"></div>
    </li>
  <?php $_from = $this->_tpl_vars['fields']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['field_info']):
?>
    <li<?php if ($this->_tpl_vars['field_info']['has_changed']): ?> class="changed_field"<?php endif; ?>>
      <div>
        <div class="sh__field_name"><?php echo $this->_tpl_vars['field_info']['field_name']; ?>
</div>
        <div class="sh__previous_value">
          <?php if ($this->_tpl_vars['has_previous_entry']): ?>
            <?php echo smarty_function_display_custom_field(array('form_id' => $this->_tpl_vars['form_id'],'submission_id' => $this->_tpl_vars['submission_id'],'field_info' => $this->_tpl_vars['field_info'],'field_types' => $this->_tpl_vars['field_types'],'value' => $this->_tpl_vars['field_info']['previous_value'],'settings' => $this->_tpl_vars['field_info']['settings'],'context' => $this->_tpl_vars['context']), $this);?>

              &nbsp;
          <?php else: ?>
            <span class="light_grey">(no previous version)</span>
          <?php endif; ?>
        </div>
        <div class="sh__new_value">
          <?php echo smarty_function_display_custom_field(array('form_id' => $this->_tpl_vars['form_id'],'submission_id' => $this->_tpl_vars['submission_id'],'field_info' => $this->_tpl_vars['field_info'],'field_types' => $this->_tpl_vars['field_types'],'value' => $this->_tpl_vars['field_info']['new_value'],'settings' => $this->_tpl_vars['field_info']['settings'],'context' => $this->_tpl_vars['context']), $this);?>

        </div>
      </div>
      <div style="clear: both"></div>
    </li>
  <?php endforeach; endif; unset($_from); ?>
  </ul>

  <table cellpadding="0" cellspacing="0">
    <tr>
      <td class="nowrap" width="60">
        <?php if ($this->_tpl_vars['previous_history_id']): ?>
          <a href="javascript:sh.view_history_changes(<?php echo $this->_tpl_vars['previous_history_id']; ?>
)"><?php echo $this->_tpl_vars['L']['word_older_leftarrow']; ?>
</a>
        <?php else: ?>
          <span class="light_grey"><?php echo $this->_tpl_vars['L']['word_older_leftarrow']; ?>
</a>
        <?php endif; ?>
      </td>
      <td class="nowrap" width="150" align="center"><a href="javascript:sh.back_to_history()"><?php echo $this->_tpl_vars['L']['phrase_back_to_history']; ?>
</a></td>
      <td class="nowrap">
        <?php if ($this->_tpl_vars['next_history_id']): ?>
          <a href="javascript:sh.view_history_changes(<?php echo $this->_tpl_vars['next_history_id']; ?>
)"><?php echo $this->_tpl_vars['L']['word_newer_rightarrow']; ?>
</a>
        <?php else: ?>
          <span class="light_grey"><?php echo $this->_tpl_vars['L']['word_newer_rightarrow']; ?>
</span>
        <?php endif; ?>
      </td>
    </tr>
  </table>

  <p>
    <input type="button" class="blue" value="<?php echo $this->_tpl_vars['L']['phrase_restore_version']; ?>
" onclick="sh.restore(<?php echo $this->_tpl_vars['item']['sh___history_id']; ?>
)" />
  </p>