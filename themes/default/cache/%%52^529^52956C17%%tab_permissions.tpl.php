<?php /* Smarty version 2.6.18, created on 2016-04-14 06:01:12
         compiled from C:%5Cxampp%5Chtdocs%5Clogbook/themes/default/../../modules/data_visualization/templates/field_charts/tab_permissions.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'upper', 'C:\\xampp\\htdocs\\logbook/themes/default/../../modules/data_visualization/templates/field_charts/tab_permissions.tpl', 1, false),array('modifier', 'in_array', 'C:\\xampp\\htdocs\\logbook/themes/default/../../modules/data_visualization/templates/field_charts/tab_permissions.tpl', 87, false),array('function', 'clients_dropdown', 'C:\\xampp\\htdocs\\logbook/themes/default/../../modules/data_visualization/templates/field_charts/tab_permissions.tpl', 43, false),)), $this); ?>
  <div class="subtitle underline margin_top_large"><?php echo ((is_array($_tmp=$this->_tpl_vars['LANG']['word_permissions'])) ? $this->_run_mod_handler('upper', true, $_tmp) : smarty_modifier_upper($_tmp)); ?>
</div>

  <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'messages.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

  <form action="<?php echo $this->_tpl_vars['same_page']; ?>
" method="post">
    <input type="hidden" name="vis_id" value="<?php echo $this->_tpl_vars['vis_id']; ?>
" />
    <input type="hidden" name="tab" value="permissions" />

    <table cellspacing="1" cellpadding="2" border="0" width="100%">
    <tr>
      <td width="130" class="medium_grey" valign="top"><?php echo $this->_tpl_vars['LANG']['phrase_access_type']; ?>
</td>
      <td>
        <table cellspacing="1" cellpadding="0" class="margin_bottom">
        <tr>
          <td>
            <input type="radio" name="access_type" id="at1" value="admin" <?php if ($this->_tpl_vars['vis_info']['access_type'] == 'admin'): ?>checked<?php endif; ?> />
              <label for="at1"><?php echo $this->_tpl_vars['LANG']['phrase_admin_only']; ?>
</label>
          </td>
        </tr>
        <tr>
          <td>
            <input type="radio" name="access_type" id="at2" value="public" <?php if ($this->_tpl_vars['vis_info']['access_type'] == 'public'): ?>checked<?php endif; ?> />
              <label for="at2"><?php echo $this->_tpl_vars['LANG']['word_public']; ?>
 <span class="light_grey"><?php echo $this->_tpl_vars['L']['phrase_all_clients_have_access']; ?>
</span></label>
          </td>
        </tr>
        <tr>
          <td>
            <input type="radio" name="access_type" id="at3" value="private" <?php if ($this->_tpl_vars['vis_info']['access_type'] == 'private'): ?>checked<?php endif; ?> />
              <label for="at3"><?php echo $this->_tpl_vars['LANG']['word_private']; ?>
 <span class="light_grey"><?php echo $this->_tpl_vars['L']['phrase_specific_clients_have_access']; ?>
</span></label>
          </td>
        </tr>
        </table>

        <div id="custom_clients" <?php if ($this->_tpl_vars['vis_info']['access_type'] != 'private'): ?>style="display:none"<?php endif; ?>>
          <table cellpadding="0" cellspacing="0" class="subpanel">
          <tr>
            <td class="medium_grey"><?php echo $this->_tpl_vars['LANG']['phrase_available_clients']; ?>
</td>
            <td></td>
            <td class="medium_grey"><?php echo $this->_tpl_vars['LANG']['phrase_selected_clients']; ?>
</td>
          </tr>
          <tr>
            <td>
              <?php echo smarty_function_clients_dropdown(array('name_id' => "available_client_ids[]",'multiple' => 'true','multiple_action' => 'hide','clients' => $this->_tpl_vars['vis_info']['client_ids'],'size' => '4','style' => "width: 220px"), $this);?>

            </td>
            <td align="center" valign="middle" width="100">
              <input type="button" value="<?php echo $this->_tpl_vars['LANG']['word_add_uc_rightarrow']; ?>
"
                onclick="ft.move_options(this.form['available_client_ids[]'], this.form['selected_client_ids[]']);" /><br />
              <input type="button" value="<?php echo $this->_tpl_vars['LANG']['word_remove_uc_leftarrow']; ?>
"
                onclick="ft.move_options(this.form['selected_client_ids[]'], this.form['available_client_ids[]']);" />
            </td>
            <td>
              <?php echo smarty_function_clients_dropdown(array('name_id' => "selected_client_ids[]",'multiple' => 'true','multiple_action' => 'show','clients' => $this->_tpl_vars['vis_info']['client_ids'],'size' => '4','style' => "width: 220px"), $this);?>

            </td>
          </tr>
          </table>
        </div>

      </td>
    </tr>
    <tr>
      <td class="medium_grey" valign="top"><?php echo $this->_tpl_vars['L']['phrase_where_shown']; ?>
</td>
      <td>

        <div>
          <input type="radio" name="access_view_mapping" id="fvm1" value="all" <?php if ($this->_tpl_vars['vis_info']['access_view_mapping'] == 'all'): ?>checked<?php endif; ?> />
          <label for="fvm1"><?php echo $this->_tpl_vars['L']['phrase_all_views']; ?>
</label>
        </div>
        <div>
          <input type="radio" name="access_view_mapping" id="fvm2" value="except" <?php if ($this->_tpl_vars['vis_info']['access_view_mapping'] == 'except'): ?>checked<?php endif; ?> />
          <label for="fvm2"><?php echo $this->_tpl_vars['L']['phrase_all_views_except']; ?>
</label>
        </div>
        <div class="margin_bottom">
          <input type="radio" name="access_view_mapping" id="fvm3" value="only" <?php if ($this->_tpl_vars['vis_info']['access_view_mapping'] == 'only'): ?>checked<?php endif; ?> />
          <label for="fvm3"><?php echo $this->_tpl_vars['L']['phrase_specific_views']; ?>
</label>
        </div>


        <div id="custom_views" <?php if ($this->_tpl_vars['vis_info']['access_view_mapping'] == 'all'): ?>style="display:none"<?php endif; ?> class="margin_top">
          <div class="grey_box">
            <ul>
            <?php $_from = $this->_tpl_vars['views']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['view_info']):
?>
              <?php $this->assign('view_id', $this->_tpl_vars['view_info']['view_id']); ?>
              <li>
                <input type="checkbox" name="view_ids[]" id="view<?php echo $this->_tpl_vars['view_id']; ?>
" value="<?php echo $this->_tpl_vars['view_id']; ?>
"
                  <?php if (((is_array($_tmp=$this->_tpl_vars['view_id'])) ? $this->_run_mod_handler('in_array', true, $_tmp, $this->_tpl_vars['access_views']) : in_array($_tmp, $this->_tpl_vars['access_views']))): ?>checked<?php endif; ?> />
                  <label for="view<?php echo $this->_tpl_vars['view_id']; ?>
"><?php echo $this->_tpl_vars['view_info']['view_name']; ?>
</label>
              </li>
            <?php endforeach; endif; unset($_from); ?>
            </ul>
          </div>
        </div>

      </td>
    </tr>
    </table>

    <p>
      <input type="button" id="delete_visualization" value="<?php echo $this->_tpl_vars['L']['phrase_delete_visualization']; ?>
" class="burgundy right" />
      <input type="submit" name="update" value="<?php echo $this->_tpl_vars['LANG']['word_update']; ?>
" />
    </p>

  </form>