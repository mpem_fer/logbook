<?php /* Smarty version 2.6.18, created on 2016-05-18 11:27:51
         compiled from C:%5Cxampp%5Chtdocs%5Clogbook/modules/data_visualization/templates/settings.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cache_frequency_dropdown', 'C:\\xampp\\htdocs\\logbook/modules/data_visualization/templates/settings.tpl', 49, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'modules_header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

  <table cellpadding="0" cellspacing="0">
  <tr>
    <td width="45"><a href="index.php"><img src="images/icon_visualization.png" border="0" width="34" height="34" /></a></td>
    <td class="title">
      <a href="../../admin/modules"><?php echo $this->_tpl_vars['LANG']['word_modules']; ?>
</a>
      <span class="joiner">&raquo;</span>
      <a href="./"><?php echo $this->_tpl_vars['L']['module_name']; ?>
</a>
      <span class="joiner">&raquo;</span>
      <?php echo $this->_tpl_vars['LANG']['phrase_main_settings']; ?>

    </td>
  </tr>
  </table>

  <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'messages.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

  <div class="margin_bottom_large">
    <?php echo $this->_tpl_vars['L']['text_activity_chart_intro']; ?>

  </div>

  <form method="post" action="<?php echo $this->_tpl_vars['same_page']; ?>
" onsubmit="return rsv.validate(this, rules)">
    <table cellspacing="0" cellpadding="1" class="list_table">
    <tr>
      <td width="230" class="pad_left_small"><?php echo $this->_tpl_vars['L']['phrase_quicklinks_dialog_default_dimensions']; ?>
</td>
      <td class="pad_left_small">
        <table cellspacing="0" cellpadding="0">
        <tr>
          <td width="60" class="medium_grey"><label for="width"><?php echo $this->_tpl_vars['L']['word_width']; ?>
</label></td>
          <td><input type="text" name="quicklinks_dialog_width" id="width" size="3" value="<?php echo $this->_tpl_vars['module_settings']['quicklinks_dialog_width']; ?>
" />px</td>
        </tr>
        <tr>
          <td class="medium_grey"><label for="height"><?php echo $this->_tpl_vars['L']['word_height']; ?>
</label></td>
          <td><input type="text" name="quicklinks_dialog_height" id="height" size="3" value="<?php echo $this->_tpl_vars['module_settings']['quicklinks_dialog_height']; ?>
" />px</td>
        </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td class="pad_left_small"><?php echo $this->_tpl_vars['L']['phrase_vis_thumb_size']; ?>
</td>
      <td>
        <input type="text" name="quicklinks_dialog_thumb_size" id="quicklinks_dialog_thumb_size" size="3"
          value="<?php echo $this->_tpl_vars['module_settings']['quicklinks_dialog_thumb_size']; ?>
" />px
      </td>
    </tr>
    <tr>
      <td class="pad_left_small"><?php echo $this->_tpl_vars['L']['phrase_default_cache_frequency']; ?>
</td>
      <td>
        <?php echo smarty_function_cache_frequency_dropdown(array('name_id' => 'default_cache_frequency','default' => $this->_tpl_vars['module_settings']['default_cache_frequency']), $this);?>

        <div class="hint">
          <?php echo $this->_tpl_vars['L']['text_cache_frequency_explanation']; ?>

        </div>
      </td>
    </tr>
    <tr>
      <td class="pad_left_small"><?php echo $this->_tpl_vars['L']['phrase_allow_clients_refresh_cache']; ?>
</td>
      <td>
        <input type="radio" name="clients_may_refresh_cache" id="cmrc1" value="yes"
          <?php if ($this->_tpl_vars['module_settings']['clients_may_refresh_cache'] == 'yes'): ?>checked<?php endif; ?> />
          <label for="cmrc1"><?php echo $this->_tpl_vars['LANG']['word_yes']; ?>
</label>
        <input type="radio" name="clients_may_refresh_cache" id="cmrc2" value="no"
          <?php if ($this->_tpl_vars['module_settings']['clients_may_refresh_cache'] == 'no'): ?>checked<?php endif; ?> />
          <label for="cmrc2"><?php echo $this->_tpl_vars['LANG']['word_no']; ?>
</label>
      </td>
    </tr>
    </table>

    <p>
      <input type="submit" name="update" value="<?php echo $this->_tpl_vars['LANG']['word_update']; ?>
" />
      <input type="submit" name="clear_cache" value="<?php echo $this->_tpl_vars['L']['phrase_clear_visualization_cache']; ?>
" />
    </p>

  </form>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'modules_footer.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>