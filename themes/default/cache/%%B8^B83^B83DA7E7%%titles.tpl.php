<?php /* Smarty version 2.6.18, created on 2016-04-14 06:03:06
         compiled from C:%5Cxampp%5Chtdocs%5Clogbook/modules/extended_client_fields/templates/titles.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'escape', 'C:\\xampp\\htdocs\\logbook/modules/extended_client_fields/templates/titles.tpl', 27, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'modules_header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

  <table cellpadding="0" cellspacing="0">
  <tr>
    <td width="45"><a href="index.php"><img src="images/icon_extended_client_fields.gif" border="0" width="34" height="34" /></a></td>
    <td class="title">
      <a href="../../admin/modules"><?php echo $this->_tpl_vars['LANG']['word_modules']; ?>
</a>
      <span class="joiner">&raquo;</span>
      <a href="./"><?php echo $this->_tpl_vars['L']['module_name']; ?>
</a>
      <span class="joiner">&raquo;</span>
      <?php echo $this->_tpl_vars['L']['phrase_section_titles']; ?>

    </td>
  </tr>
  </table>

  <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'messages.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

  <div class="margin_bottom_large">
    <?php echo $this->_tpl_vars['L']['text_client_field_titles_intro']; ?>

  </div>

  <form action="<?php echo $this->_tpl_vars['same_page']; ?>
" method="post">

    <table cellspacing="1" cellpadding="1" border="0">
    <tr>
      <td width="190" class="medium_grey"><?php echo $this->_tpl_vars['L']['phrase_main_account_top']; ?>
</td>
      <td><input type="text" style="width:200px" name="main_account_page_top_title" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['main_account_page_top_title'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" /></td>
    </tr>
    <tr>
      <td class="medium_grey"><?php echo $this->_tpl_vars['L']['phrase_main_account_middle']; ?>
</td>
      <td><input type="text" style="width:200px" name="main_account_page_middle_title" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['main_account_page_middle_title'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" /></td>
    </tr>
    <tr>
      <td class="medium_grey"><?php echo $this->_tpl_vars['L']['phrase_main_account_bottom']; ?>
</td>
      <td><input type="text" style="width:200px" name="main_account_page_bottom_title" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['main_account_page_bottom_title'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" /></td>
    </tr>
    <tr>
      <td class="medium_grey"><?php echo $this->_tpl_vars['L']['phrase_settings_page_top']; ?>
</td>
      <td><input type="text" style="width:200px" name="settings_page_top_title" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['settings_page_top_title'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" /></td>
    </tr>
    <tr>
      <td class="medium_grey"><?php echo $this->_tpl_vars['L']['phrase_settings_page_bottom']; ?>
</td>
      <td><input type="text" style="width:200px" name="settings_page_bottom_title" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['settings_page_bottom_title'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" /></td>
    </tr>
    </table>

    <p>
      <input type="submit" name="update" value="<?php echo $this->_tpl_vars['LANG']['word_update']; ?>
" />
    </p>

  </form>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'modules_footer.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>