<?php /* Smarty version 2.6.18, created on 2016-04-14 05:58:17
         compiled from C:%5Cxampp%5Chtdocs%5Clogbook/modules/data_visualization/templates/field_charts/edit.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'ft_include', 'C:\\xampp\\htdocs\\logbook/modules/data_visualization/templates/field_charts/edit.tpl', 16, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'modules_header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

  <table cellpadding="0" cellspacing="0" class="margin_bottom_large">
  <tr>
    <td width="45"><a href="../index.php"><img src="../images/icon_visualization.png" border="0" width="34" height="34" /></a></td>
    <td class="title">
      <a href="../../../admin/modules"><?php echo $this->_tpl_vars['LANG']['word_modules']; ?>
</a>
      <span class="joiner">&raquo;</span>
      <a href="../"><?php echo $this->_tpl_vars['L']['module_name']; ?>
</a>
      <span class="joiner">&raquo;</span>
      <?php echo $this->_tpl_vars['L']['phrase_edit_field_chart']; ?>

    </td>
  </tr>
  </table>

  <?php echo smarty_function_ft_include(array('file' => 'tabset_open.tpl'), $this);?>


    <?php if ($this->_tpl_vars['page'] == 'main'): ?>
      <?php echo smarty_function_ft_include(array('file' => '../../modules/data_visualization/templates/field_charts/tab_main.tpl'), $this);?>

    <?php elseif ($this->_tpl_vars['page'] == 'appearance'): ?>
      <?php echo smarty_function_ft_include(array('file' => '../../modules/data_visualization/templates/field_charts/tab_appearance.tpl'), $this);?>

    <?php elseif ($this->_tpl_vars['page'] == 'permissions'): ?>
      <?php echo smarty_function_ft_include(array('file' => '../../modules/data_visualization/templates/field_charts/tab_permissions.tpl'), $this);?>

    <?php elseif ($this->_tpl_vars['page'] == 'advanced'): ?>
      <?php echo smarty_function_ft_include(array('file' => '../../modules/data_visualization/templates/field_charts/tab_advanced.tpl'), $this);?>

    <?php else: ?>
      <?php echo smarty_function_ft_include(array('file' => '../../modules/data_visualization/templates/field_charts/tab_main.tpl'), $this);?>

    <?php endif; ?>

  <?php echo smarty_function_ft_include(array('file' => 'tabset_close.tpl'), $this);?>


<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'modules_footer.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>