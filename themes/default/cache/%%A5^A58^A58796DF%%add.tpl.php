<?php /* Smarty version 2.6.18, created on 2016-04-14 05:57:10
         compiled from C:%5Cxampp%5Chtdocs%5Clogbook/modules/data_visualization/templates/field_charts/add.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'upper', 'C:\\xampp\\htdocs\\logbook/modules/data_visualization/templates/field_charts/add.tpl', 44, false),array('function', 'forms_dropdown', 'C:\\xampp\\htdocs\\logbook/modules/data_visualization/templates/field_charts/add.tpl', 56, false),array('function', 'colour_dropdown', 'C:\\xampp\\htdocs\\logbook/modules/data_visualization/templates/field_charts/add.tpl', 113, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'modules_header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

  <table cellpadding="0" cellspacing="0">
  <tr>
    <td width="45"><a href="../index.php"><img src="../images/icon_visualization.png" border="0" width="34" height="34" /></a></td>
    <td class="title">
      <a href="../../../admin/modules"><?php echo $this->_tpl_vars['LANG']['word_modules']; ?>
</a>
      <span class="joiner">&raquo;</span>
      <a href="../"><?php echo $this->_tpl_vars['L']['module_name']; ?>
</a>
      <span class="joiner">&raquo;</span>
      <?php echo $this->_tpl_vars['L']['phrase_new_field_chart']; ?>

    </td>
  </tr>
  </table>

  <?php if ($this->_tpl_vars['g_message']): ?>

    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "messages.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

    <div><b><?php echo $this->_tpl_vars['L']['word_actions']; ?>
</b></div>
    <ul>
      <li><a href="../"><?php echo $this->_tpl_vars['L']['phrase_list_visualizations']; ?>
</a></li>
      <li><a href="add.php"><?php echo $this->_tpl_vars['L']['phrase_create_new_field_chart']; ?>
</a></li>
      <?php if ($this->_tpl_vars['g_success']): ?>
        <li><a href="edit.php?vis_id=<?php echo $this->_tpl_vars['vis_id']; ?>
"><?php echo $this->_tpl_vars['L']['phrase_edit_this_field_chart']; ?>
</a></li>
      <?php endif; ?>
      <?php if ($this->_tpl_vars['form_id'] && $this->_tpl_vars['view_id']): ?>
        <li><a href="../../../admin/forms/submissions.php?form_id=<?php echo $this->_tpl_vars['form_id']; ?>
&view_id=<?php echo $this->_tpl_vars['view_id']; ?>
"><?php echo $this->_tpl_vars['L']['phrase_view_form_submissions']; ?>
</a></li>
      <?php endif; ?>
    </ul>

  <?php else: ?>

    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "messages.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "../../modules/data_visualization/no_internet_connection.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

    <div class="margin_bottom_large">
      <?php echo $this->_tpl_vars['L']['text_add_visualization']; ?>

    </div>

	  <form action="<?php echo $this->_tpl_vars['same_page']; ?>
" method="post" onsubmit="return rsv.validate(this, rules)">

	    <div class="subtitle underline margin_bottom_large"><?php echo ((is_array($_tmp=$this->_tpl_vars['LANG']['phrase_main_settings'])) ? $this->_run_mod_handler('upper', true, $_tmp) : smarty_modifier_upper($_tmp)); ?>
</div>

	    <table cellspacing="1" cellpadding="0" class="list_table margin_bottom_large">
	    <tr>
	      <td class="pad_left_small" width="180"><?php echo $this->_tpl_vars['L']['phrase_visualization_name']; ?>
</td>
	      <td>
	        <input type="text" name="vis_name" id="vis_name" value="" style="width: 99%" />
	      </td>
	    </tr>
	    <tr>
	      <td class="pad_left_small"><?php echo $this->_tpl_vars['LANG']['word_form']; ?>
</td>
	      <td>
	        <?php echo smarty_function_forms_dropdown(array('name_id' => 'form_id','omit_forms' => $this->_tpl_vars['omit_forms'],'include_blank_option' => true), $this);?>

	      </td>
	    </tr>
	    <tr>
	      <td class="pad_left_small"><?php echo $this->_tpl_vars['LANG']['word_view']; ?>
</td>
	      <td>
	        <select name="view_id" id="view_id" disabled>
	          <option value=""><?php echo $this->_tpl_vars['LANG']['phrase_please_select_form']; ?>
</option>
	        </select>
	      </td>
	    </tr>
      <tr>
        <td class="pad_left_small"><?php echo $this->_tpl_vars['LANG']['word_field']; ?>
</td>
        <td>
          <select name="field_id" id="field_id" disabled>
            <option value=""><?php echo $this->_tpl_vars['L']['phrase_please_select_view']; ?>
</option>
          </select>
        </td>
      </tr>
	    </table>

	    <table cellspacing="0" cellpadding="0" width="100%" class="margin_bottom_large">
	    <tr>
	      <td valign="top">

	        <div class="subtitle underline margin_bottom_large"><?php echo ((is_array($_tmp=$this->_tpl_vars['L']['word_appearance'])) ? $this->_run_mod_handler('upper', true, $_tmp) : smarty_modifier_upper($_tmp)); ?>
</div>

	        <table cellspacing="0" cellpadding="1" class="list_table margin_bottom_large">
	        <tr>
	          <td class="pad_left_small"><?php echo $this->_tpl_vars['L']['phrase_chart_type']; ?>
</td>
	          <td>
	            <input type="radio" name="chart_type" id="ct1" value="pie_chart"
	              <?php if ($this->_tpl_vars['module_settings']['field_chart_default_chart_type'] == 'pie_chart'): ?>checked<?php endif; ?> />
	              <label for="ct1"><?php echo $this->_tpl_vars['L']['phrase_pie_chart']; ?>
</label>
	            <input type="radio" name="chart_type" id="ct2" value="bar_chart"
	              <?php if ($this->_tpl_vars['module_settings']['field_chart_default_chart_type'] == 'bar_chart'): ?>checked<?php endif; ?> />
	              <label for="ct2"><?php echo $this->_tpl_vars['L']['phrase_bar_chart']; ?>
</label>
	            <input type="radio" name="chart_type" id="ct3" value="column_chart"
	              <?php if ($this->_tpl_vars['module_settings']['field_chart_default_chart_type'] == 'column_chart'): ?>checked<?php endif; ?> />
	              <label for="ct3"><?php echo $this->_tpl_vars['L']['phrase_column_chart']; ?>
</label>
	          </td>
	        </tr>
	        <tr>
	          <td class="pad_left_small"><?php echo $this->_tpl_vars['L']['phrase_ignore_fields_with_empty_vals']; ?>
</td>
	          <td>
	            <input type="radio" name="field_chart_ignore_empty_fields" id="ief1" value="yes"
	              <?php if ($this->_tpl_vars['module_settings']['field_chart_ignore_empty_fields'] == 'yes'): ?>checked<?php endif; ?> />
	              <label for="ief1"><?php echo $this->_tpl_vars['LANG']['word_yes']; ?>
</label>
	            <input type="radio" name="field_chart_ignore_empty_fields" id="ief2" value="no"
	              <?php if ($this->_tpl_vars['module_settings']['field_chart_ignore_empty_fields'] == 'no'): ?>checked<?php endif; ?> />
	              <label for="ief2"><?php echo $this->_tpl_vars['LANG']['word_no']; ?>
</label>
	          </td>
	        </tr>
          <tr>
            <td class="pad_left_small"><?php echo $this->_tpl_vars['L']['word_colour']; ?>
</td>
            <td>
              <input type="hidden" name="colour_old" value="<?php echo $this->_tpl_vars['module_settings']['field_chart_colour']; ?>
" />
              <?php echo smarty_function_colour_dropdown(array('name_id' => 'colour','default' => $this->_tpl_vars['module_settings']['field_chart_colour']), $this);?>

            </td>
          </tr>
	        </table>

          <div class="subtitle underline margin_bottom_large"><?php echo ((is_array($_tmp=$this->_tpl_vars['L']['phrase_pie_chart_settings'])) ? $this->_run_mod_handler('upper', true, $_tmp) : smarty_modifier_upper($_tmp)); ?>
</div>

	        <table cellspacing="0" cellpadding="1" class="list_table">
	        <tr>
	          <td width="190" class="pad_left_small"><?php echo $this->_tpl_vars['L']['phrase_pie_chart_format']; ?>
</td>
	          <td>
	            <input type="radio" name="pie_chart_format" id="pcf1" value="2D"
	              <?php if ($this->_tpl_vars['module_settings']['field_chart_pie_chart_format'] == '2D'): ?>checked<?php endif; ?>
	              <?php if ($this->_tpl_vars['module_settings']['field_chart_default_chart_type'] != 'pie_chart'): ?>disabled<?php endif; ?> />
	              <label for="pcf1">2D</label>
	            <input type="radio" name="pie_chart_format" id="pcf2" value="3D"
	              <?php if ($this->_tpl_vars['module_settings']['field_chart_pie_chart_format'] == '3D'): ?>checked<?php endif; ?>
	              <?php if ($this->_tpl_vars['module_settings']['field_chart_default_chart_type'] != 'pie_chart'): ?>disabled<?php endif; ?> />
	              <label for="pcf2">3D</label>
	          </td>
	        </tr>
	        <tr>
	          <td class="pad_left_small"><?php echo $this->_tpl_vars['L']['phrase_include_legend_in_thumbnail']; ?>
</td>
	          <td>
	            <input type="radio" name="include_legend_quicklinks" id="ilq1" value="yes"
	              <?php if ($this->_tpl_vars['module_settings']['field_chart_include_legend_quicklinks'] == 'yes'): ?>checked<?php endif; ?>
	              <?php if ($this->_tpl_vars['module_settings']['field_chart_default_chart_type'] != 'pie_chart'): ?>disabled<?php endif; ?> />
	              <label for="ilq1"><?php echo $this->_tpl_vars['LANG']['word_yes']; ?>
</label>
	            <input type="radio" name="include_legend_quicklinks" id="ilq2" value="no"
	              <?php if ($this->_tpl_vars['module_settings']['field_chart_include_legend_quicklinks'] == 'no'): ?>checked<?php endif; ?>
	              <?php if ($this->_tpl_vars['module_settings']['field_chart_default_chart_type'] != 'pie_chart'): ?>disabled<?php endif; ?> />
	              <label for="ilq2"><?php echo $this->_tpl_vars['LANG']['word_no']; ?>
</label>
	          </td>
	        </tr>
	        <tr>
	          <td class="pad_left_small"><?php echo $this->_tpl_vars['L']['phrase_include_legend_in_full_size']; ?>
</td>
	          <td>
	            <input type="radio" name="include_legend_full_size" id="ilf1" value="yes"
	              <?php if ($this->_tpl_vars['module_settings']['field_chart_include_legend_full_size'] == 'yes'): ?>checked<?php endif; ?>
	              <?php if ($this->_tpl_vars['module_settings']['field_chart_default_chart_type'] != 'pie_chart'): ?>disabled<?php endif; ?> />
	              <label for="ilf1"><?php echo $this->_tpl_vars['LANG']['word_yes']; ?>
</label>
	            <input type="radio" name="include_legend_full_size" id="ilf2" value="no"
	              <?php if ($this->_tpl_vars['module_settings']['field_chart_include_legend_full_size'] == 'no'): ?>checked<?php endif; ?>
	              <?php if ($this->_tpl_vars['module_settings']['field_chart_default_chart_type'] != 'pie_chart'): ?>disabled<?php endif; ?> />
	              <label for="ilf2"><?php echo $this->_tpl_vars['LANG']['word_no']; ?>
</label>
	          </td>
	        </tr>
	        </table>

	      </td>
	      <td width="250" valign="top">
          <div class="subtitle underline margin_bottom_large">&nbsp;<?php echo ((is_array($_tmp=$this->_tpl_vars['L']['word_thumbnail'])) ? $this->_run_mod_handler('upper', true, $_tmp) : smarty_modifier_upper($_tmp)); ?>
</div>
	        <div id="thumb_chart" style="display:none"><?php echo $this->_tpl_vars['L']['phrase_select_form_and_field']; ?>
</div>
	        <span id="thumb_chart_empty" class="medium_grey">&nbsp;<?php echo $this->_tpl_vars['L']['phrase_select_form_and_field']; ?>
</span>
	      </td>
	    </tr>
	    </table>

      <div class="subtitle underline margin_bottom_large"><?php echo $this->_tpl_vars['L']['phrase_full_size']; ?>
</div>
      <div id="full_size_chart" style="display:none"></div>
        <span id="full_size_chart_empty" class="medium_grey"><?php echo $this->_tpl_vars['L']['phrase_select_form_and_field']; ?>
</span>

	    <p>
	      <input type="submit" name="add" value="<?php echo $this->_tpl_vars['L']['phrase_create_visualization']; ?>
" />
	    </p>
	  </form>

  <?php endif; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'modules_footer.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>